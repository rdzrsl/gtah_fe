import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { Router} from '@angular/router';
import { AuthService } from './auth.service';
import * as moment from 'moment-timezone';
import { Ng4FilesConfig } from '../ng4-files';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  previousUrl: any;
  currentUrl: any;
  at:any;
  u_id:any;
  u_type:any;
  co_id:any;
  isAgent: boolean = false;
  isHq: boolean = false;
  isFinance: boolean = false;
  pageNum: any = 1;
  pageSize: any = 5;
  userTypesData: any = [];
  allPackageIncludesData: any = [];
  loader: boolean = false;
  isView: boolean = false;
  monthSelArr = [
    { "name": "January", "value": 1},
    { "name": "February", "value": 2},
    { "name": "March", "value": 3},
    { "name": "April", "value": 4},
    { "name": "May", "value": 5},
    { "name": "June", "value": 6},
    { "name": "July ", "value": 7},
    { "name": "August", "value": 8},
    { "name": "September", "value": 9},
    { "name": "October", "value": 10},
    { "name": "November", "value": 11},
    { "name": "December", "value": 12}
  ];

  constructor(
    private common:CommonService, 
    private router:Router, 
    private auth: AuthService) {

      this.at = this.auth.at;
      this.u_id = this.auth.u_id;
      this.co_id = this.auth.co_id;
  }

  getPages(maxPageVal) {
    let res;
    let tempArr = [];
    // res = Math.ceil(maxVal/this.pageSize);
    for(let i=0; i<maxPageVal; i++) {
      tempArr.push(i+1);
    }
    res = tempArr;

    return res;
  }

  timeLeftChecker(due,current, type?) {
    let ddate, cdate, tl;

    ddate = moment(new Date(due));
    cdate = moment(new Date(current));

    if(type != 'number')
      tl = ddate.from(cdate);
    else
      tl = ddate.diff(cdate, 'days');

    return tl;
  }

  getIncludeItemNameByID(id) {
    let res = '';
    this.allPackageIncludesData.forEach(el => {
      if(el.id == id)
        res = el.name;
    });
    return res;
  }

  getUserTypeNameByID(userTypeID) {
    let res;
    this.userTypesData.forEach(el => {
      if(el.id == userTypeID)
        res = el.name;
    });
    return res;
  }

  getPreviousUrl() {
    return this.previousUrl;
  }

  getCurrSessData(type) {
    let res;

    if(type == 'at')
      res = this.at;
    if(type == 'uid')
      res = this.u_id;
    if(type == 'coid')
      res = this.co_id;
      
    return res;
  }

  checkSession() {

    let valid, legit;
    
    if(localStorage.getItem('at')) {
      valid = this.auth.validateUT(localStorage.getItem('at'));

      // if(valid === true) {
        // console.log('authenticated');
        legit = true;
      } else {
        localStorage.removeItem('at');
        localStorage.removeItem('co_id');
        localStorage.removeItem('u_id');
        localStorage.removeItem('id');
        localStorage.removeItem('name');
        localStorage.removeItem('email');
        localStorage.removeItem('end');
  
        if(localStorage.getItem('ut'))
          localStorage.removeItem('ut');
  
        this.auth.at = null;
        this.auth.co_id = null;
        this.auth.u_id = null;
        
        this.common.isLogin = false;
  
        this.router.navigate(['/']);
        // location.reload(true);
        window.location.href=window.location.href;
      // }
        legit = false;
    }
    return legit;
  }
}
