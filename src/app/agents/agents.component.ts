import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '../services/data.service';
import * as moment from 'moment-timezone';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.scss'],
  // animations: [
  //   trigger('listStagger',[
  //     transition('* <=> *', [
  //       query(':enter', [
  //         style({ opacity: 0, transform: 'translateY(-15px)'}),
  //         stagger('50ms',
  //         animate('550ms ease-out',
  //         style({ opacity: 1, transform: 'translateY(0px)'})))
  //       ], {optional: true}),
  //       query(':leave', [
  //         animate('50ms',
  //         style({ opacity: 0}))
  //       ], {optional: true})

  //     ])
  //   ])
  // ]
})
export class AgentsComponent implements OnInit, AfterViewInit {

  agents:any = [];
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0 ;
  displayedColumns: string[] = ['num', 'name', 'icno', 'status', 'action'];
  dataSource: MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dsvc:DataService,
    public shared: SharedService) {
    moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
    this.dataSource = new MatTableDataSource(this.agents);
    this.shared.loader = true;
  }

  ngOnInit() {
    // this.getAgents();
    this.getAgents(this.shared.pageNum,999);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getAgents(page?,size?) {

    let paginationUrl;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }

    this.agents = [];
    this.dsvc.getAll('user/type/4/1'+paginationUrl).subscribe(
      data => {
        this.agents = data['users'];

        this.agents.forEach((el,index) => {
          el.row = index;
          el.type = this.shared.getUserTypeNameByID(el.type);
          el.created_at = moment( el.created_at).format('DD-MM-YYYY h:mm A');
          el.updated_at = moment(el.updated_at).format('DD-MM-YYYY h:mm A');
        });
        this.dataSource.data = this.agents;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.shared.loader = false;
        
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };
  }

  removeItem(id, name) {
    if(confirm("Delete: "+name+'?')) {
      this.dsvc.delUser(id).subscribe(
        data => {
          setTimeout(() => {
            this.getAgents(this.shared.pageNum,this.shared.pageSize);
          }, 1000);
          // this.agents = data
      }),
      error => {
        alert('Data unreachable!');
        console.log({'err':error});
      };

    } else {
    }
  }

}
