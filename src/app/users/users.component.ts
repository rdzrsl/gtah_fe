import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '../services/data.service';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import * as moment from 'moment-timezone';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  // animations: [
  //   trigger('listStagger',[
  //     transition('* <=> *', [
  //       query(':enter', [
  //         style({ opacity: 0, transform: 'translateY(-15px)'}),
  //         stagger('50ms',
  //         animate('550ms ease-out',
  //         style({ opacity: 1, transform: 'translateY(0px)'})))
  //       ], {optional: true}),
  //       query(':leave', [
  //         animate('50ms',
  //         style({ opacity: 0}))
  //       ], {optional: true})

  //     ])
  //   ])
  // ]
})
export class UsersComponent implements OnInit, AfterViewInit {
  
  users: any = [];
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0 ;
  displayedColumns: string[] = ['num', 'name', 'contact_no', 'email', 'role', 'status', 'action'];
  dataSource: MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private dsvc:DataService,
    public shared: SharedService,
    ) {
    // moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
    this.dataSource = new MatTableDataSource(this.users);
    this.shared.loader = true;
  }

  ngOnInit() {
    // this.getUsers();
    this.getUsers(this.shared.pageNum,999);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getUsers(page?,size?) {

    let paginationUrl;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }

    let arr:any = [];
    this.dsvc.getAll('user'+paginationUrl).subscribe(
      data => {
        
        arr = data['result']['data'];

        arr.forEach(function(item,index)  {
          item.row = index;
        });

        arr.forEach(el => {
          // this.dsvc.getUserTypeByID(el.type).subscribe(data => {
            el.type = this.shared.getUserTypeNameByID(el.type);
            
            el.created_at = moment( el.created_at).format('DD-MM-YYYY h:mm A');
            el.updated_at = moment(el.updated_at).format('DD-MM-YYYY h:mm A');
          }),
          error => {
            alert('Data unreachable!');
            console.log({'err':error});
          };
        
        this.users = arr;
        // this.dataSource = data['result'];

        // this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.data = this.users;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.shared.loader = false;
        
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };

  }

  removeItem(id, name) {
    if(confirm("Delete: "+name+'?')) {
      this.dsvc.delUser(id).subscribe(
        data => {
        setTimeout(() => {
          this.getUsers(this.shared.pageNum,this.shared.pageSize);
        }, 1000);
      }),
      error => {
        alert('Data unreachable!');
        console.log({'err':error});
      };

    } else {
    }
  }

}
