import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';
import { MatDatepickerInputEvent, DateAdapter, MAT_DATE_FORMATS, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/adapter/date.adapter';
import {
  Ng4FilesStatus,
  Ng4FilesSelected,
  Ng4FilesService,
  Ng4FilesConfig
} from './../../ng4-files';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

export interface DialogData {
  src: string;
}

@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.scss'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class PackageComponent implements OnInit {

  refId: any;
  getData: Object;
  includes: any = [];
  excludes: any = [];
  src2modal: string;
  nullPointers: any = [];
  events: string[] = [];
  urlStorage = environment.storageURL;

  updateForm: FormGroup;

  template: FormControl; 
  code: FormControl; 
  name: FormControl; 
  user: FormControl; 
  includes_id: FormControl; 
  excludes_id: FormControl; 
  co_id: FormControl; 
  unit: FormControl; 
  adult_price: FormControl; 
  childbed_price: FormControl; 
  childnobed_price: FormControl; 
  infant_price: FormControl; 
  description: FormControl; 
  cc_id: FormControl; 
  travel_date: FormControl; 
  return_date: FormControl;
  mediaFile: FormControl;
  active: FormControl;
  created_at: any; 
  updated_at: any;
  isActive: any;
  pageMode: string;
  complete: boolean;
  sMinDate: Date;
  eMinDate: Date;
  td: number;
  rd: number;
  // isAgent: boolean = false;
  selectedFiles;
  selFiles = [];
  showImg: boolean;
  base64textString: string;
  media: any = [];
  incArr = [];
  exArr = [];
  strDuration: string;
  sdt: number;
  edt: number;
  packages: any = [];
  commconfigs: any = [];
  filteredOptions: Observable<any[]>;
  selFiles1 = [];
  invalidDate: boolean;
  selCommConfig: any;
  cc_idSel: any;
  pkgCode: any;

  constructor(
    public dialog: MatDialog,
    private router:Router,
    private dsvc:DataService,
    private toastr: ToastrService,
    private ng4FilesService: Ng4FilesService,
    public shared: SharedService
    ) {
      moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
      this.shared.loader = true;
      //  this.shared.checkSession();;
  }

  public fileConfig: Ng4FilesConfig = {
    acceptExtensions: [
      'png', 
      'PNG', 
      'jpg', 
      'JPG', 
      'jpeg',
      'JPEG'
    ],
    maxFilesCount: 5,
    maxFileSize: 5120000,
    totalFilesSize: 5120000
  };

  ngOnInit() {
    
    this.ng4FilesService.addConfig(this.fileConfig);

    this.refId = this.router.url.split('/')[3];

    this.template = new FormControl();
    this.code = new FormControl();
    this.name = new FormControl();
    this.user = new FormControl();
    this.includes_id = new FormControl();
    this.excludes_id = new FormControl();
    this.co_id = new FormControl(); 
    this.unit = new FormControl();
    this.adult_price = new FormControl(); 
    this.childbed_price = new FormControl(); 
    this.childnobed_price = new FormControl(); 
    this.infant_price = new FormControl(); 
    this.cc_id = new FormControl(); 
    this.description = new FormControl(); 
    this.mediaFile = new FormControl(); 
    this.active = new FormControl();
    this.travel_date = new FormControl();
    this.return_date = new FormControl();

    this.updateForm = new FormGroup ({
      template : this.template,
      code : this.code,
      name : this.name,
      user : this.user,
      includes_id : this.includes_id,
      excludes_id : this.excludes_id,
      co_id : this.co_id,
      unit : this.unit,
      cc_id : this.cc_id,
      adult_price : this.adult_price,
      childbed_price : this.childbed_price,
      childnobed_price : this.childnobed_price,
      infant_price : this.infant_price,
      description : this.description,
      mediaFile : this.mediaFile,
      active : this.active,
      travel_date : this.travel_date,
      return_date : this.return_date,
    });
    
    this.getIncludes();
    this.getExcludes();
    this.getPackageList();
    this.getCommConfigList();
    this.updateForm.get('active').setValue(false);
    this.updateForm.get('excludes_id').disable();

    this.sMinDate = new Date(new Date().getFullYear(),new Date().getMonth(), new Date().getDate());

    if(this.refId === 'add') {
      this.pageMode = 'ADD';
      this.eMinDate = new Date(new Date().getFullYear(),new Date().getMonth(), new Date().getDate());
    } else {
      this.getRow(this.refId);
      this.pageMode = 'EDIT';
      this.template.disable();
      this.code.disable();
      this.cc_id.disable();
    }
    this.checkReqValues();
    
    // this.complete = false;
  }

  private _filter(value: any): any[] {
    let filterValue = value.toLowerCase();

    return this.packages.filter(packages => packages.toLowerCase().indexOf(filterValue) === 0);
  }

  getPackageList() {

    let arr:any = [];
    this.dsvc.getAll('package').subscribe(
      data => {
        
        arr = data;

        arr.forEach(el => {
          this.packages.push(el.code);
        });

        this.filteredOptions = this.template.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );

        // console.log({'packages':this.packages});
        
    }),
    error => {
      alert('Data unreachable!');
      // console.log({'err':error});
    };
  }

  getCommConfigList() {

    let arr:any = [];
    this.dsvc.getAll('commconfig/active').subscribe(
      data => {
        
        this.commconfigs = data;

        // arr.forEach(el => {
        //   this.commconfigs.push(el.name);
        // });

        // console.log({'packages':this.packages});
        
    }),
    error => {
      alert('Data unreachable!');
      // console.log({'err':error});
    };
  }

  showConfig(cc) {
    // console.log({'cc':cc});
    this.selCommConfig = cc;
    
  }

  getCommConfig(id) {
    let res;

    this.commconfigs.forEach(el => {
      if(el.id == id)
        res = el;
    });
    return res;
  }

  setForm(code) {
    
    // console.log(code);
    if(code != 0) {
      this.getRowByCode(code);
    } else {
      this.updateForm.reset();
    }
    
  }

  removeImage(id,name) {

    // console.log({'id':id});
    

    if(confirm("Delete: "+name+'?')) {
      this.dsvc.delete('media',id).subscribe(
      ),
      error => {
        alert('Data unreachable!');
      };
      this.getRow(this.refId);
      // this.getAllMedia(this.refId);
    } else {
    }
  }

  removeAllImage(id, elem?) {

    if(confirm("Delete all images?")) {
      this.dsvc.delete('media/package',id).subscribe(
      ),
      error => {
        alert('Data unreachable!');
      };
      this.media = [];
      this.updateForm.get(elem).setValue('');
      this.getRow(this.refId);
      // this.getAllMedia(this.refId);
    } 
    // else {
    // }
  }
  
  validateCode(code) {
    if(this.pageMode == 'ADD') {
    this.shared.loader = true;
    this.dsvc.getByID('package/validate/code',code, true)
    .subscribe(
      Rdata => {
        // console.log({'code exists?':Rdata});
        if(Rdata == true){
          this.code.setErrors({'incorrect': true});
          this.code.setValue('');
        }
        this.shared.loader = false;
        
      }, err => {
            this.shared.loader = false;
      });
    }
  }

  getRowByCode(code) {
    
    this.shared.loader = true;

    // Update ErrorMsg Service
    this.dsvc.getByID('package/view',code, true)
    .subscribe(
      Rdata => {
        
        // this.commonservice.errorHandling(Rdata, (function(){
        this.getData = Rdata[0];
      
        this.eMinDate = new Date(new Date(this.getData['travel_date']).getFullYear(),new Date(this.getData['travel_date']).getMonth(), new Date(this.getData['travel_date']).getDate()+1);
        
        // populate data
        // this.updateForm.get('code').setValue(this.getData['code']);
        this.updateForm.get('name').setValue(this.getData['name']);
        this.updateForm.get('user').setValue(this.getData['user']);
        this.updateForm.get('includes_id').setValue(JSON.parse(this.getData['includes_id']));
        this.updateForm.get('excludes_id').setValue(JSON.parse(this.getData['excludes_id']));
        this.updateForm.get('co_id').setValue(this.getData['co_id']);
        this.updateForm.get('unit').setValue(this.getData['unit']);

        // this.getDuration(this.getData['travel_date'], this.getData['return_date']);
        // this.updateForm.get('duration').setValue(this.strDuration);

        // this.updateForm.get('duration').setValue(this.getData['duration']);
        this.updateForm.get('adult_price').setValue(this.getData['adult_price']);
        this.updateForm.get('childbed_price').setValue(this.getData['childbed_price']);
        this.updateForm.get('childnobed_price').setValue(this.getData['childnobed_price']);
        this.updateForm.get('infant_price').setValue(this.getData['infant_price']);
        this.updateForm.get('description').setValue(this.getData['description']);
        this.updateForm.get('travel_date').setValue(moment(this.getData['travel_date']).tz("Asia/Kuala_Lumpur").toISOString());

        if(this.sdt == undefined)
          this.sdt = moment(this.getData['travel_date']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');

        this.updateForm.get('return_date').setValue(moment(this.getData['return_date']).tz("Asia/Kuala_Lumpur").toISOString());

        if(this.edt == undefined)
          this.edt = moment(this.getData['return_date']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');
          
        this.getDuration(this.sdt,this.edt);

        this.updateForm.get('active').setValue((this.getData['active'] == 1)?true:false);

        this.isActive = this.getData['active'];
        this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
        this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

        // this.getAllMedia(this.getData['id']);
        
        this.updateList();
        this.incArr = [];
        this.exArr = [];
        
        JSON.parse(this.getData['includes_id']).forEach(el1 => {
          this.incArr.push(this.shared.getIncludeItemNameByID(el1));
        });
        
        JSON.parse(this.getData['excludes_id']).forEach(el2 => {
          this.exArr.push(this.shared.getIncludeItemNameByID(el2));
      });

        this.checkReqValues();
      // }).bind(this));
    }, err => {
          this.shared.loader = false;
    });
    
  }

  getRow(id) {
    
    this.shared.loader = true;

    // Update ErrorMsg Service
    this.dsvc.getPackage(id)
    .subscribe(
      Rdata => {
        
        // this.commonservice.errorHandling(Rdata, (function(){
        this.getData = Rdata['body'];
      
        this.eMinDate = new Date(new Date(this.getData['travel_date']).getFullYear(),new Date(this.getData['travel_date']).getMonth(), new Date(this.getData['travel_date']).getDate()+1);
        
        // populate data
        if(this.getData['code']){
          this.pkgCode = this.getData['code'];
          this.updateForm.get('code').setValue(this.getData['code']);
        }
        this.updateForm.get('name').setValue(this.getData['name']);
        // this.updateForm.get('user').setValue(this.getData['user']);
        this.updateForm.get('includes_id').setValue(JSON.parse(this.getData['includes_id']));
        this.updateForm.get('excludes_id').setValue(JSON.parse(this.getData['excludes_id']));
        this.updateForm.get('co_id').setValue(this.getData['co_id']);
        this.updateForm.get('unit').setValue(this.getData['unit']);

        if(this.getData['cc_id']){
          this.cc_idSel = this.getData['cc_id'];
          this.updateForm.get('cc_id').setValue(this.getCommConfig(this.getData['cc_id']));
          this.showConfig(this.getCommConfig(this.getData['cc_id']));
        }

        // this.getDuration(this.getData['travel_date'], this.getData['return_date']);
        // this.updateForm.get('duration').setValue(this.strDuration);

        // this.updateForm.get('duration').setValue(this.getData['duration']);
        this.updateForm.get('adult_price').setValue(this.getData['adult_price']);
        this.updateForm.get('childbed_price').setValue(this.getData['childbed_price']);
        this.updateForm.get('childnobed_price').setValue(this.getData['childnobed_price']);
        this.updateForm.get('infant_price').setValue(this.getData['infant_price']);
        this.updateForm.get('description').setValue(this.getData['description']);
        this.updateForm.get('travel_date').setValue(moment(this.getData['travel_date']).tz("Asia/Kuala_Lumpur").toISOString());

        if(this.sdt == undefined)
          this.sdt = moment(this.getData['travel_date']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');

        this.updateForm.get('return_date').setValue(moment(this.getData['return_date']).tz("Asia/Kuala_Lumpur").toISOString());

        if(this.edt == undefined)
          this.edt = moment(this.getData['return_date']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');
          
        this.getDuration(this.sdt,this.edt);

        this.updateForm.get('active').setValue((this.getData['active'] == 1)?true:false);

        this.isActive = this.getData['active'];
        this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
        this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

        this.getAllMedia(this.refId);
        
        this.updateList();
        
        // JSON.parse(this.getData['includes_id']).forEach(el1 => {
        //   this.incArr.push(this.shared.getIncludeItemNameByID(el1));
        // });
        
        // JSON.parse(this.getData['excludes_id']).forEach(el2 => {
        //   this.exArr.push(this.shared.getIncludeItemNameByID(el2));
        // });

        this.checkReqValues();
      // }).bind(this));
    }, err => {
          this.shared.loader = false;
    });
    
  }

  checkDateRange(start,end) {
    let res;
    
    res = moment(start).isSameOrAfter(end);

    return res;
  }

  setMinDate(sDate) {
    let year, month, day;
    
    year = new Date(sDate).getFullYear();
    month = new Date(sDate).getMonth();
    day = new Date(sDate).getDate()+1;

    this.eMinDate = new Date(year,month,day);
  }

  addStartEvent(type: string, event: MatDatepickerInputEvent<Date>) { 
    // console.log('addStart');
    // console.log({'event':event.value});

    this.events = [];
    this.events.push(`${event.value}`);
    this.sdt = moment(this.events[0]).tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD');

    this.setMinDate(this.sdt);
    
    if (this.sdt && this.edt){
      // console.log({'sdt':this.sdt});
      // console.log({'edt':this.edt});
      this.invalidDate = this.checkDateRange(this.sdt,this.edt);
      console.log({'s this.invalidDate':this.invalidDate});

      if(this.invalidDate)
        this.updateForm.get('return_date').setValue(null);

    } else {
      // console.log({'sdt':this.sdt});
    }

    // this.edt = new Date(year,month,day).getTime();
    //this.updateForm.get('end').setValue(new Date(this.edt).toISOString());

    this.checkReqValues();
  }

  addEndEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    // console.log('addEnd');
    // console.log({'event':event.value});

    this.events = [];
    this.events.push(`${event.value}`);
    this.edt = moment(this.events[0]).tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD');
    
    if (this.sdt && this.edt){
      // console.log({'sdt':this.sdt});
      // console.log({'edt':this.edt});
      this.invalidDate = this.checkDateRange(this.sdt,this.edt);
      // console.log({'e this.invalidDate':this.invalidDate});
    } else {
      // console.log({'edt':this.edt});
    }
    
    this.checkReqValues()
  }

  getDuration(travelDate, returnDate) {
    // console.log({'travelDate':travelDate});
    // console.log({'returnDate':returnDate});
    
    let d, n, tyear, tmonth, tday, ryear, rmonth, rday, days, nights;

    this.strDuration = null;

    tyear = parseInt(travelDate.split("-")[2]);
    // tmonth = parseInt(travelDate.split("-")[1]);
    tday = parseInt(travelDate.split("-")[0]);
    // console.log({'tyear':tyear});
    // console.log({'tmonth':tmonth});
    // console.log({'tday':tday});
    
    ryear = parseInt(returnDate.split("-")[2]);
    // rmonth = parseInt(returnDate.split("-")[1]);
    rday = parseInt(returnDate.split("-")[0]);
    // console.log({'ryear':ryear});
    // console.log({'rmonth':rmonth});
    // console.log({'rday':rday});

    d = moment([tyear, 0, tday]);
    n = moment([ryear, 0, rday]);
    // console.log({'d':d});
    // console.log({'n':n});

    nights = n.diff(d, 'days');
    // console.log({'nights':nights});
    days = n.diff(d, 'days')+1;
    // console.log({'days':days});

    this.strDuration = days+'D'+nights+'N';
    // this.updateForm.get('duration').setValue(this.strDuration);
    // console.log({'strDuration':this.strDuration});
    
    
  }

  getAllMedia(id) {
    let arrFileNames = [];

    this.shared.loader = true;
    // Update ErrorMsg Service
    this.dsvc.getByID('media/package', id)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
          this.media = Rdata[0];

          this.media.forEach(el => {

            arrFileNames.push(el.name);
          
          });

          this.updateForm.controls.mediaFile.setValue(arrFileNames.join(", "));
          
          // }).bind(this));
          this.shared.loader = false;
        }, err => {
          this.shared.loader = false;
          // this.shared.loader = false;
    });
  }

  filesSelect(selectedFiles: Ng4FilesSelected): void {

    let mFileSize = this.fileConfig.maxFileSize;
    
    let fileExtn = selectedFiles.files[0].name.split('.')[1];

    if (selectedFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {     
      if (selectedFiles.files.length > 0 && mFileSize) {      
        
            let arrFileNames = [];
            this.selFiles = [];
            let file2convert;
            var file:File;
            let tempArr = [];
            let extension = null;
            let mediaType = null;

            selectedFiles.files.forEach(el => {
  
              arrFileNames.push(el.name);
              extension = el.name.split('.')[1];
              mediaType = this.getFileCode(extension);

              file = el;
              let myReader:FileReader = new FileReader();
            
              myReader.onloadend = (e) => {
                file2convert = myReader.result;
                tempArr.push({"path":null, "name":el.name, "size":el.size, "type":el.type, "ext":extension, "code":mediaType, "src":file2convert});
              }
              myReader.readAsDataURL(file);
            });
            this.selFiles = tempArr;

            this.updateForm.controls.mediaFile.setValue(arrFileNames.join(", "));

            this.checkReqValues();
            this.showImg = true;

          }else{
            this.toastr.error('File Size Exceed maximum file size','');
          }
  
        } else if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
          this.toastr.error('Maximum files count exceed.Please upload one file', '');
        } else if (selectedFiles.status === Ng4FilesStatus.STATUS_MAX_FILE_SIZE_EXCEED) {
          this.toastr.error('Maximum files size exceed', '');
    }

    this.selectedFiles = Array.from(selectedFiles.files).map(file => file.name);

    // this.selFiles.push(selectedFiles);
  }

  getIncludes() {

    this.shared.loader = true;
    // Update ErrorMsg Service
    this.dsvc.getAll('packageincludes/active')
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
          this.includes = Rdata;
          
          this.includes.forEach(el => {
            el.selected = false;
            el.disabled = false;
          });
          // console.log({'includes':this.includes});

          // }).bind(this));
          this.shared.loader = false;
        }, err => {
          this.shared.loader = false;
          // this.shared.loader = false;
    });
    
  }

  getExcludes() {

    this.shared.loader = true;
    // Update ErrorMsg Service
    this.dsvc.getAll('packageincludes/active')
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
          this.excludes = Rdata;
          
          this.excludes.forEach(el => {
            el.selected = false;
            el.disabled = false;
          });
          // console.log({'excludes':this.excludes});
          // this.updateForm.get('excludes_id').disable();
      
          // }).bind(this));
          this.shared.loader = false;
        }, err => {
          this.shared.loader = false;
          // this.shared.loader = false;
    });
  }

  updateList() {
    
    // INCLUDES
      this.includes.forEach((el1) => this.includes_id.value.forEach((el2) => {
          
        if(el1.id == el2) {
            el1.selected = true;
        }
        
      }));
      
      if(this.includes_id.value.length > 0) {

        this.updateForm.get('excludes_id').enable();
        this.excludes.forEach((el1) => this.includes_id.value.forEach((el2) => {
          
          if(el1.id == el2) {
            el1.disabled = true;
          }
          
        }));

      } else {
        
        this.excludes.forEach(el => {
          el.disabled = false;
        });
        this.updateForm.get('excludes_id').disable();
      }
      
      if(this.excludes_id.value && this.excludes_id.value.length > 0) {

        this.includes.forEach((el1) => this.excludes_id.value.forEach((el2) => {
          
          if(el1.id == el2) {
            el1.disabled = true;
          }
          
        }));

      } else {
        
        this.includes.forEach(el => {
          el.disabled = false;
        });
      }
      this.shared.loader = false;

      this.showSelected();
  }

  showSelected() {

    this.incArr = [];
    this.exArr = [];
    // console.log({'incArr':this.incArr});
    // console.log({'exArr':this.incArr});
    // console.log({'includes_id.value':this.includes_id.value});
    // console.log({'excludes_id.value':this.excludes_id.value});

      if(this.includes_id.value) {
        this.includes_id.value.forEach(el1 => {
          this.incArr.push(this.shared.getIncludeItemNameByID(el1));
        });
      }

      if(this.excludes_id.value) {
        this.excludes_id.value.forEach(el2 => {
          this.exArr.push(this.shared.getIncludeItemNameByID(el2));
        });
      }

    this.checkReqValues();
  }

  clear(elem) {
    if(elem == 'mediaFile') {
      this.removeAllImage(this.refId,elem);
    } else if(elem == 'excludes_id') {
      this.excludes_id.reset();
      this.exArr = [];
        
      this.updateList();
    } else {
      this.updateForm.get(elem).setValue('');
    }
  }

  back(module) {
      this.router.navigate([module])
  }

  getFileCode(ext) {
    let exist, res;

    exist = this.fileConfig.acceptExtensions.includes(ext.toLowerCase());
    
    res = exist;

    if(exist) {
      switch (ext) {
        case 'docx':
          res = 'word';
          break;

        case 'doc':
          res = 'word';
          break;

        case 'pdf':
          res = 'pdf';
          break;

        case 'png':
          res = 'image';
          break;

        case 'jpg':
          res = 'image';
          break;

        case 'jpeg':
          res = 'image';
          break;
      
        default:
          break;
      }
    }

    return res;
  }

  checkReqValues() {

    let reqVal: any = [];

    if(this.pageMode == 'ADD') {
      reqVal = ['code', 'name', 'cc_id', 'description', 'unit', 'travel_date', 'return_date', 'includes_id', 'adult_price', 'mediaFile'];
    } else {
      reqVal = ['name', 'description', 'unit', 'travel_date', 'return_date', 'includes_id', 'adult_price'];
    }
    let nullPointers: any = [];

    for (var reqData of reqVal) {
      let elem = this.updateForm.get(reqData);

      if (elem.value == "" || elem.value == null) {
        elem.setValue(null)
        nullPointers.push(null)
      }
    }

    if (nullPointers.length > 0) {
      this.complete = false;
    } else {
      this.complete = true;
    }
    // console.log({'nullPointers.length':nullPointers.length});
    

  }

  uploader() {
    
    let formData: FormData = new FormData();

    // console.log({'selFiles1 length':this.selFiles1.length});
    if(this.selFiles1.length != 0) {
        // console.log({'selFiles1':this.selFiles1[0]});
        
        // formData.append('mediaFile', this.selectedFiles);
        
        // formData.append('mediaFile', this.selFiles1[0].files, this.selFiles1[0].files.name);
        this.selFiles1[0].files.forEach(el => {
          // console.log({'el':el});
          formData.append('mediaFile', el);
        });
    }
      
    // Add Service
    this.dsvc.uploadFiles(formData)
    .subscribe(res => {
      // console.log({'res':res.body[0]});
      
      this.toastr.success(res.body[0], '');
      // this.router.navigate(['protected/package']);
      this.shared.loader = false;
    }),
    error => {
      this.toastr.error('Error! '+error, '');
      this.shared.loader = false;
    };
  }

  submit(formValues:any) {
        
    // console.log({'formValues':formValues});

    this.shared.loader = true;

    let body = {
      id: null,
      co_id: null,
      code: null,
      name: null,
      description: null,
      cc_id: null,
      adult_price: null,
      childbed_price: null,
      childnobed_price: null,
      infant_price: null,
      includes_id: null,
      excludes_id: null,
      unit: null,
      user: null,
      travel_date: null,
      return_date: null,
      files: null,
      active: null
    };

    formValues.includes_id = JSON.stringify(formValues.includes_id);
    formValues.excludes_id = JSON.stringify(formValues.excludes_id);

    body.co_id = this.shared.co_id;
    body.user = this.shared.u_id;


    if(this.refId !== 'add') {
    
    body.id = this.refId;
    body.code = this.pkgCode;
    body.name = formValues.name;
    body.description = formValues.description;
    body.cc_id = parseInt(this.cc_idSel);
    body.adult_price = parseFloat(formValues.adult_price);
    body.childbed_price = parseFloat(formValues.childbed_price);
    body.childnobed_price = parseFloat(formValues.childnobed_price);
    body.infant_price = parseFloat(formValues.infant_price);
    body.includes_id = formValues.includes_id;
    body.excludes_id = formValues.excludes_id;
    body.unit = parseInt(formValues.unit);
    body.travel_date = moment(formValues.travel_date).format('YYYY-MM-DD hh:mm:ss');
    body.return_date = moment(formValues.return_date).format('YYYY-MM-DD hh:mm:ss');
    body.active = formValues.active;
    body.files = this.selFiles;
        
    // console.log({'body':body});

    // Update service
      this.dsvc.putPackage(body, parseInt(this.refId))
      .subscribe(res => {

        this.toastr.success('Update Successful!', '');
        this.router.navigate(['protected/package']);
        this.shared.loader = false;
      }),
      error => {
        this.toastr.error('Error! '+error, '');
        this.shared.loader = false;
      };

    } else {
    
      body.name = formValues.name;
      body.code = formValues.code;
      body.description = formValues.description;
      body.cc_id = parseInt(formValues.cc_id.id);
      body.adult_price = parseFloat(formValues.adult_price);
      body.childbed_price = parseFloat(formValues.childbed_price);
      body.childnobed_price = parseFloat(formValues.childnobed_price);
      body.infant_price = parseFloat(formValues.infant_price);
      body.includes_id = formValues.includes_id;
      body.excludes_id = formValues.excludes_id;
      body.unit = parseInt(formValues.unit);
      body.travel_date = moment(formValues.travel_date).format('YYYY-MM-DD hh:mm:ss');
      body.return_date = moment(formValues.return_date).format('YYYY-MM-DD hh:mm:ss');
      body.active = formValues.active;
      body.files = this.selFiles;
        
      // console.log({'body':body});
      
      // Add Service
      this.dsvc.addPackage(body)
      .subscribe(res => {
        this.toastr.success('Add Successful!', '');
        this.router.navigate(['protected/package']);
        this.shared.loader = false;
      }),
      error => {
        this.toastr.error('Error! '+error, '');
        this.shared.loader = false;
      };

    }
  }

  openDialog(imgSrc): void {

      this.src2modal = this.urlStorage+'/package/'+this.refId+'/'+imgSrc;

    const dialogRef = this.dialog.open(ViewPackageDialog, {
      width: '800px',
      height: '600px',
      data: {'src': this.src2modal}
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}

@Component({
  selector: 'view-dialog',
  templateUrl: './view-dialog.html',
  styleUrls: ['./package.component.scss']
})
export class ViewPackageDialog {

  constructor(
    public dialogRef: MatDialogRef<ViewPackageDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
