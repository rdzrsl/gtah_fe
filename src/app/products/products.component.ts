import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
  animations: [
    trigger('listStagger',[
      transition('* <=> *', [
        query(':enter', [
          style({ opacity: 0, transform: 'translateY(-15px)'}),
          stagger('50ms',
          animate('550ms ease-out',
          style({ opacity: 1, transform: 'translateY(0px)'})))
        ], {optional: true}),
        query(':leave', [
          animate('50ms',
          style({ opacity: 0}))
        ], {optional: true})

      ])
    ])
  ]
})
export class ProductsComponent implements OnInit {

  products: Object;

  constructor(private dsvc:DataService,
    public shared: SharedService) {
    }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.dsvc.getProductsByCompanyID(localStorage.getItem('co_id')).subscribe(
      data => this.products = data
    ),
    error => {
      alert('Data unreachable!');
      console.log({'err':error});
    };
  }
  // removeItem(id, name) {
  //   if(confirm("Delete: "+name+'?')) {
  //     this.dsvc.delPackage(id).subscribe(
  //       data => this.packages = data
  //     ),
  //     error => {
  //       alert('Data unreachable!');
  //       console.log({'err':error});
  //     };
  //     this.getList();
  //   } else {
  //   }
  // }

}