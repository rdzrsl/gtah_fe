import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { DataService } from '../services/data.service';
import * as moment from 'moment-timezone';
import { Router, NavigationEnd } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets  } from 'chart.js';
import { Label, Color } from 'ng2-charts';
// declare var jquery: any;
// declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  baseColors = {
    // aqua: "#00ffff",
    // azure: "#f0ffff",
    // beige: "#f5f5dc",
    // black: "#000000",
    // brown: "#a52a2a",
    // cyan: "#00ffff",
    // darkblue: "#00008b",
    // darkcyan: "#008b8b",
    // darkgrey: "#a9a9a9",
    // darkgreen: "#006400",
    // darkkhaki: "#bdb76b",
    // darkmagenta: "#8b008b",
    // darkolivegreen: "#556b2f",
    // darkorange: "#ff8c00",
    // darkorchid: "#9932cc",
    // darkred: "#8b0000",
    // darksalmon: "#e9967a",
    // darkviolet: "#9400d3",
    fuchsia: "#ff00ff",
    // brown: "#a52a2a",
    // gold: "#ffd700",
    green: "#008000",
    // indigo: "#4b0082",
    // khaki: "#f0e68c",
    // lightblue: "#add8e6",
    // lightcyan: "#e0ffff",
    lightgreen: "#90ee90",
    // lightgrey: "#d3d3d3",
    // lightpink: "#ffb6c1",
    // lightyellow: "#ffffe0",
    // lime: "#00ff00",
    // violet: "#800080",
    // brown: "#a52a2a",
    gold: "#ffd700",
    blue: "#0000ff",
    // magenta: "#ff00ff",
    // maroon: "#800000",
    // navy: "#000080",
    // olive: "#808000",
    orange: "#ffa500",
    pink: "#ffc0cb",
    purple: "#800080",
    red: "#ff0000",
    silver: "#c0c0c0"
  };

  chart10colors: any = [
    this.baseColors.blue,
    this.baseColors.gold,
    this.baseColors.green,
    this.baseColors.lightgreen,
    this.baseColors.fuchsia,
    this.baseColors.orange,
    this.baseColors.pink,
    this.baseColors.purple,
    this.baseColors.red,
    this.baseColors.silver
  ];
  
  name: any = "";
  email: any;
  co_id: any;
  company: any = [];
  agents: any = [];
  products: any = [];
  packages: any = [];
  etgcommission: any = [];
  hqcommission: any = [];
  opscommission: any = [];
  userID: number;
  userType: number;
  agentcommission: any = [];
  rptCurrMonth: any = [];
  agentcommissionPaging: any = [];
  previousTopAgents: any = [];
  previousMonth = moment().tz("Asia/Kuala_Lumpur").months()-1;
  
  prv: boolean = false;
  nxt: boolean = false;
  currPage: any;
  lastPage: any;

  public barChartData: ChartDataSets[] = [];
  public hbarChartData: ChartDataSets[] = [];
  public lineChartData: ChartDataSets[] = [];
  public lineChartData1: ChartDataSets[] = [];
  maxValDaily: number;
  maxValMonthly: number;
  
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true,
                // ticks: {
                //   stepSize: 1,
                //   min: 0,
                //   max: 10
                // }
            }]
        }
  };
  
  public hbarChartOptions: (ChartOptions) = {
    responsive: true,
    scales: {
            xAxes: [{
              display: false,
              stacked: false,
              ticks: {
                stepSize: 1,
                min: 0,
                max: 10
              }
            }],
            yAxes: [{
                display: true,
                stacked: false,
                // ticks: {
                //   stepSize: 1,
                //   min: 0,
                //   max: 10
                // }
            },]
        },
        tooltips: {
          enabled: false,
          intersect: false
        },
        legend: {
          display: false
        }
  };
  
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    scales: {
        yAxes: [{
            ticks: {
                // Include a dollar sign in the ticks
                // callback: function(value, index, values) {
                //     return value;
                // },
                stepSize: 1,
                min: 0,
                max: 10
            }
        }]
      }
  };
  
  public lineChartOptions1: (ChartOptions) = {
    responsive: true,
    scales: {
        yAxes: [{
            ticks: {
                // Include a dollar sign in the ticks
                // callback: function(value, index, values) {
                //     return value;
                // },
                stepSize: 1,
                min: 0,
                max: 10
            }
        }]
    },
    elements: {
      line: {
          tension: 0 // disables bezier curves
      }
    }
  };
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];
  
  public hbarChartLabels: Label[] = [

  ];
  public barChartLabels: Label[] = [
    'Jan', 
    'Feb', 
    'Mar', 
    'Apr', 
    'May', 
    'Jun', 
    'Jul', 
    'Aug', 
    'Sep', 
    'Oct', 
    'Nov', 
    'Dec'
  ];

  public dailyData = {data:[], fill: false, label: '-' };
  public monthlyData = {data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], fill: false, label: '-' };

  public lineChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public hbarChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public lineChartColors: Color[] = [
    {
      // borderColor: 'black',
      // backgroundColor: 'transparent',
    },
  ];

  totalAgents: any;
  iv: any;
  maxBarVal: any;
  userReport: any;
  // hbarChartData: any;

  constructor(
    private router: Router,
    public shared: SharedService,
    private dsvc:DataService
  ) {
    this.shared.currentUrl = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.shared.previousUrl = this.shared.currentUrl;
      }
    }); 
    moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
    this.shared.loader = true;
  }

  ngOnInit() {

    this.previousMonth = moment().month(this.previousMonth).format('MMMM');
    
    this.chart10colors.forEach(ce => {
      this.lineChartColors.push(
        {'backgroundColor':ce,
        'borderColor': ce}
        );
    });
    // this.lineChartColors = this.chart10colors;
    
    // console.log({'days in month': moment().tz("Asia/Kuala_Lumpur").daysInMonth()});
    // console.log({'last day of month': moment().tz("Asia/Kuala_Lumpur").endOf("month").subtract(0, 'days').format('DD-MMM-YYYY')});

    this.getReportByPackageSales();
    this.genCurrentMonthDays();

    if(localStorage.getItem('name'))
      this.name = localStorage.getItem('name').toString();

    if(localStorage.getItem('email'))
      this.email = localStorage.getItem('email').toString();
      
    if(localStorage.getItem('co_id'))
      this.co_id = parseInt(localStorage.getItem('co_id'));

    if(localStorage.getItem('u_id'))
      this.userID = parseInt(localStorage.getItem('u_id'));
    
    if(localStorage.getItem('ut'))
      this.userType = parseInt(localStorage.getItem('ut'));

    this.getCompanyInfo();
    this.getCompanyPackageList();
    this.getReportByUser(this.userID);
    
    if(this.userType != 4) {
      this.getAgentList();
      this.getCommissionsByPartyList('egt', this.shared.pageNum, this.shared.pageSize);
      this.getCommissionsByPartyList('hq', this.shared.pageNum, this.shared.pageSize);
      this.getCommissionsByPartyList('ops', this.shared.pageNum, this.shared.pageSize);
      this.getCommissionsByPartyList('agent', this.shared.pageNum, this.shared.pageSize);
    } else {
      this.getCommissionsByPartyList('', this.shared.pageNum, this.shared.pageSize);
    }

    // console.log({'u_type':this.shared.u_type});
    // console.log({'shared.type': this.shared.u_type});
    
    this.iv = setInterval(() => {

      this.barChartData = [];
      this.hbarChartData = [];
      this.hbarChartLabels = [];
      this.lineChartLabels = [];
      this.lineChartData = [];
      this.lineChartData1 = [];
      this.getReportByPackageSales();
      this.genCurrentMonthDays();
    }, 300000);
    
  }

  ngAfterViewInit() {
    // this.context = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');
  }

  ngOnDestroy() {
    if(this.iv)
      clearInterval(this.iv);
 
  }

  genCurrentMonthDays() {
    // let currMonthTotalDays = moment().tz("Asia/Kuala_Lumpur").endOf("month").format('DD-MMM-YYYY');
    let currMonthTotalDays = moment().tz("Asia/Kuala_Lumpur").daysInMonth()+1;
    // console.log({'currMonthTotalDays':currMonthTotalDays});

    // this.dailyData.data.push(4);
    // this.dailyData.data.push(6);
    for(let i = 1; i < currMonthTotalDays; i++) {
      this.lineChartLabels.push(moment().tz("Asia/Kuala_Lumpur").format(i+'-MMM-YYYY'));
      this.dailyData.data.push(0);
    }
    
    // this.lineChartData.push(this.dailyData);
    // console.log({'genCurrentMonthDays dailyData':this.dailyData});
    
    // this.getReportByCurrMonthSales();
    this.getCurrMonthReportByAgentSalesPerformance();
    
  }

  getAgentList() {
    
    this.dsvc.getUsersByType(4).subscribe(
      data => {
        // console.log({'getAgentList data':data});
        let list;
        // this.agents = data;
        list = data['users'];
        this.totalAgents = data['total'];

        list.forEach((e, i) => {
          if(i < 5) {
            e.created_at = moment(new Date(e.created_at)).format('DD-MM-YYYY');
            this.agents.push(e);
          }
        });
        
        this.shared.loader = false;
    },
    error => {
      this.shared.loader = false;
      console.log({'err':error.statusText});
    });

  }

  getCompanyPackageList() {
    this.dsvc.getPackageByCompanyID(localStorage.getItem('co_id')).subscribe(
      data => {
        let arrSum = [];
        this.packages = data['data'];
        this.packages.forEach(el => {
          arrSum.push(el.unit);
          el.travel_date = moment(el.travel_date).format('DD-MM-YYYY');
          el.return_date = moment(el.return_date).format('DD-MM-YYYY');
        });
        // let sum =  arrSum.reduce((acc, cur) => acc + cur, 0);
        this.packages.total = data['total'];
        // console.log({'this.packages':this.packages.total});
        this.shared.loader = false;
    },
    error => {
      this.shared.loader = false;
      console.log({'err':error.statusText});
    });
  }

  getCommissionsByPartyList(party, page?, size?) {

    let paginationUrl;
    let getUrl;

    if(this.userType != 4 && party != '') 
      getUrl = 'commission/'+party;
    else
      getUrl = 'commission/user/paging/'+this.userID+'/'+this.userType;
  
      if(page && size) {
        paginationUrl = `?page=${page}&size=${size}`;
      } else {
        paginationUrl = '';
      }

    // console.log({'getUrl':getUrl});
    
    this.dsvc.getAll(getUrl+paginationUrl).subscribe(
      data => {
        if(party == 'egt') {
          this.etgcommission = data['data']['result']['data'];
          this.etgcommission.total = this.numberWithCommas(data['total']);
          this.etgcommission.forEach(el => {
            el.created_at = moment(el.created_at).format('DD-MM-YYYY h:mm A');
  
            // if(el.etg_status == 1)
            //   el.etg_status = "Paid";
            // else
            //   el.etg_status = "Unpaid";
          });
        } else if(party == 'hq') {
          this.hqcommission = data['data']['result']['data'];
          this.hqcommission.total = this.numberWithCommas(data['total']);
          this.hqcommission.forEach(el => {
            el.created_at = moment(el.created_at).format('DD-MM-YYYY h:mm A');
  
            // if(el.hq_status == 1)
            //   el.hq_status = "Cleared";
            // else
            //   el.hq_status = "Uncleared";
          });
        } else if(party == 'ops') {
          this.opscommission = data['data']['result']['data'];
          this.opscommission.forEach(el => {
            el.created_at = moment(el.created_at).format('DD-MM-YYYY h:mm A');
  
            // if(el.ops_status == 1)
            //   el.ops_status = "Cleared";
            // else
            //   el.ops_status = "Uncleared";
          });
        } else if(party == 'agent') {
          this.agentcommission = data['data']['result']['data'];
          this.agentcommission.total = this.numberWithCommas(data['total']);
          this.agentcommission.unpaid = this.numberWithCommas(data['unpaid']);
          this.agentcommission.paid = this.numberWithCommas(data['paid']);
          this.agentcommission.forEach(el => {
            el.created_at = moment(el.created_at).format('DD-MM-YYYY h:mm A');
  
            // this.agentcommissionPaging = this.shared.getPages(data['data']['result']['last_page']);
            // this.currPage = data['data']['result']['current_page'];
            // this.lastPage = data['data']['result']['last_page'];
            // this.nxt = true;
            // this.prv = false;
            
            // console.log({'this.agentcommissionPaging':this.agentcommissionPaging});
          });
        } else if(party == '') {
          this.agentcommission = data['result']['data'];
          this.agentcommission.forEach(el => {
            el.created_at = moment(el.created_at).format('DD-MM-YYYY h:mm A');
            
          });
        }
        // let arrSum = [];
        // this.etgcommission = data;
        // let sum =  arrSum.reduce((acc, cur) => acc + cur, 0);
        // this.etgcommission.total = sum;
        // console.log({'this.etgcommission':this.etgcommission});
        this.shared.loader = false;
        
    }),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

  getReportByPackageSales() {
    this.dsvc.getAll('booking/monthlyreport/package').subscribe(
      data => {

        // console.log({'data':data});

        let tempMonthlyData = this.clone(this.monthlyData.data);

        let tempMonthlyDataLabel = this.clone(this.monthlyData.label);
        
        let tempMonthly = { 'data': tempMonthlyData, 'label': tempMonthlyDataLabel};
        
        // console.log({'tempMonthly':tempMonthly});
        
        this.rptCurrMonth = [];
        this.rptCurrMonth = data;

        // loop available agents
        this.rptCurrMonth.forEach(el => {
          let tempData = this.clone(tempMonthlyData);

            el.count = this.reformCount(el.count);

            this.monthlyData.data = this.clone(tempData);
          
            // loop available counts
            el.count.forEach(el1 => {
              let exist = Object.keys(this.monthlyData.data).includes(el1.date);

              if(exist) {
                let ind = parseInt(el1.date);
                this.monthlyData.data[ind-1] = el1.count;
                this.monthlyData.label = el.code;
              }

              // this.maxValMonthly = Math.max.apply(null, this.monthlyData.data);
            });


            // console.log({'maxValMonthly':this.maxValMonthly});
            
  
            // this.barChartOptions = {
            //   responsive: true,
            //   scales: {
            //           xAxes: [{
            //               stacked: true
            //           }],
            //           yAxes: [{
            //             stacked: true,
            //             ticks: {
            //                 stepSize: 1,
            //                 min: 0,
            //                 max: this.maxValMonthly+5
            //             }
            //         }]
            //       }
            // };

            this.barChartData.push(this.monthlyData);

            this.monthlyData = this.clone(tempMonthly);
          });

          // console.log({'barChartData':this.barChartData });
          
        this.shared.loader = false;
      }
    ),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

  getReportByCurrMonthSales() {
    let temp:any = [];

    this.dsvc.getByID('booking/monthlyreport/'+localStorage.getItem('u_id'), localStorage.getItem('ut')).subscribe(
      data => {

        // console.log({'data':data});

        let tempDailyData = this.clone(this.dailyData.data);

        let tempDailyDataLabel = this.clone(this.dailyData.label);
        
        let tempDaily = { 'data': tempDailyData, fill: false, 'label': tempDailyDataLabel};
        
        // console.log({'tempDaily':tempDaily});

        this.rptCurrMonth = data['line'].slice(0,10);

        // loop available agents
        this.rptCurrMonth.forEach(el => {
          let tempData = this.clone(tempDailyData);

            el.count = this.reformCount(el.count);

            this.dailyData.data = this.clone(tempData);
          
            // loop available counts
            el.count.forEach(el1 => {
              let exist = Object.keys(this.dailyData.data).includes(el1.date);

              if(exist) {
                let ind = parseInt(el1.date);
                this.dailyData.data[ind-1] = el1.count;
                this.dailyData.label = el.name;
              }

            });

            // temp.push(0);
            this.maxValDaily = Math.max.apply(null, this.dailyData.data);
            // console.log({'temp':temp});
            // console.log(this.maxValDaily);

            this.lineChartOptions = {
              responsive: true,
              scales: {
                  yAxes: [{
                      ticks: {
                          // Include a dollar sign in the ticks
                          // callback: function(value, index, values) {
                          //     return value;
                          // },
                          stepSize: 1,
                          min: 0,
                          max: this.maxValDaily+5
                      }
                  }]
                }
            };

            // this.lineChartOptions1 = {
            //   responsive: true,
            //   scales: {
            //       yAxes: [{
            //           display: false,
            //           ticks: {
            //               // Include a dollar sign in the ticks
            //               // callback: function(value, index, values) {
            //               //     return value;
            //               // },
            //               stepSize: 1,
            //               min: 0,
            //               max: this.maxValDaily+5
            //           }
            //       }]
            //   },
            //   elements: {
            //     line: {
            //         tension: 0 // disables bezier curves
            //     }
            //   },
            //   tooltips: {
            //     enabled: false
            //   }
            // };

            this.lineChartData.push(this.dailyData);
            // console.log({'lineChartData1':this.lineChartData1});

            this.dailyData = this.clone(tempDaily);
          });
          
        this.shared.loader = false;
      }
    ),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

  getCurrMonthReportByAgentSalesPerformance() {
    let temp:any = [];

    this.dsvc.getByID('booking/monthlyreport/'+localStorage.getItem('u_id'), 2).subscribe(
      data => {

        // console.log({'data':data});

        this.previousTopAgents = data['prev'];
        this.hbarChartLabels = data['bar'].label;
        this.hbarChartData.push({'data':data['bar'].data, backgroundColor: this.chart10colors });

        // BAR CHART SEGMENT START
        // data['bar'].forEach((elBar, index) => {

        //   this.hbarChartLabels.push(elBar.label);
        //   temp.push(elBar.data[0]);

        // });
        // this.hbarChartData.push({ 'data':temp});
        // console.log({'temp':temp});
        // console.log({'hbarChartLabels':this.hbarChartLabels});
        // console.log({'hbarChartData':this.hbarChartData});

        this.maxBarVal = Math.max.apply(null, this.hbarChartData[0].data);

        this.hbarChartOptions = {
          responsive: true,
          // animation: {
          //   onComplete: function () {
          //       var chartInstance = this.chart,
          //       ctx = chartInstance.ctx;
          //       ctx.textAlign = 'center';
          //       ctx.textBaseline = 'middle';
          //       this.data.datasets.forEach(function (dataset, i) {
          //           var meta = chartInstance.controller.getDatasetMeta(i);
          //           meta.data.forEach(function (bar, index) {
          //               var data = 'RM '+dataset.data[index].toLocaleString();
          //               ctx.fillText(data, bar._model.x + 50, bar._model.y);
          //           });
          //       });
          //   }
          // },
          scales: {
                  xAxes: [{
                    display: false,
                    stacked: false,
                    ticks: {
                      stepSize: 1,
                      min: 0,
                      max: this.maxBarVal+1000
                    }
                  }],
                  yAxes: [{
                    display: true,
                      stacked: false,
                      // ticks: {
                      //   callback: function(value, index, values) {
                      //       return value;
                      //   },
                      // }
                  }]
              },
              tooltips: {
                enabled: false,
                intersect: false,
              },
              legend: {
                display: false
              }
        };

        // this.maxValDaily = null;

        // BAR CHART SEGMENT END

        let tempDailyData = this.clone(this.dailyData.data);

        let tempDailyDataLabel = this.clone(this.dailyData.label);
        
        let tempDaily = { 'data': tempDailyData, fill: false, 'label': tempDailyDataLabel};

        // data['line'].slice(0,10).forEach((le,index) => {
        //   console.log({'le':le.name});
        
        // });
        //   console.log({'sliced':data['line'].slice(0,10).length});
        
        this.rptCurrMonth = data['line'].slice(0,10);
        
        // loop available agents
        this.rptCurrMonth.forEach(el => {
          // console.log({'el':el});

          let tempData = this.clone(tempDailyData);

            el.count = this.reformCount(el.count);

            this.dailyData.data = this.clone(tempData);
          
            // // loop available counts
            el.count.forEach(el1 => {
              // console.log({'el1':el1});

                let exist = Object.keys(this.dailyData.data).includes(el1.date);
                
                let ind = parseInt(el1.date);
                this.dailyData.label = el.label;
                // this.dailyData[ind].backgroundColor = this.getRandomColor();
                
                if(exist) {
                  this.dailyData.data[ind-1] = el1.count;
                } 
            });

            this.maxValDaily = Math.max.apply(null, this.dailyData.data);
            // console.log({'temp':temp});
            // console.log(this.maxValDaily);

            this.lineChartOptions1 = {
              responsive: true,
              scales: {
                  yAxes: [{
                      display: false,
                      ticks: {
                          fontColor: '#FFF',
                          // Include a dollar sign in the ticks
                          // callback: function(value, index, values) {
                          //     return value;
                          // },
                          stepSize: 1,
                          min: 0,
                          max: this.maxValDaily+7
                      }
                  }]
              },
              elements: {
                line: {
                    tension: 0 // disables bezier curves
                },
                point: { radius: 0 }
              },
              tooltips: {
                enabled: false,
                intersect: false
              }
            };

            this.lineChartData.push(this.dailyData);
            this.lineChartData1.push(this.dailyData);
            this.lineChartData1[0].backgroundColor = this.baseColors.silver;
            this.lineChartData1[0].borderColor = this.baseColors.silver;
            
            this.dailyData = this.clone(tempDaily);
          });

          // this.lineChartData1.forEach((data) => this.dailyData.data.forEach((ddata) => {
          //   if(data > ddata)
          //     ddata = data;
          //   else if(data == ddata)
          //     data = ddata;
          // }));

          // console.log({'hbarChartData':this.hbarChartData});
          // console.log({'lineChartData1':this.lineChartData1});
          // console.log({'lineChartData1[0].data':this.lineChartData1[0].data});
          
        this.shared.loader = false;
      }
    ),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

  getReportByUser(id) {
    
    this.dsvc.getByID('report/user',id).subscribe(
      data => {
        this.userReport = data;
        // console.log({'userReport data':data});

        if(this.userReport.overallSales)
          this.userReport.overallSales = this.numberWithCommas(this.userReport.overallSales);
        else
          this.userReport.overallSales = 0;

        if(this.userReport.overallCommission)
          this.userReport.overallCommission = this.numberWithCommas(this.userReport.overallCommission);
        else
          this.userReport.overallCommission = 0;

        if(this.userReport.currentMonthSales)
          this.userReport.currentMonthSales = this.numberWithCommas(this.userReport.currentMonthSales);
        else
          this.userReport.currentMonthSales = 0;

        if(this.userReport.currentMonthCommission)
          this.userReport.currentMonthCommission = this.numberWithCommas(this.userReport.currentMonthCommission);
        else
          this.userReport.currentMonthCommission = 0;


        this.shared.loader = false;
    },
    error => {
      this.shared.loader = false;
      console.log({'err':error.statusText});
    });

  }

  // events
  // public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
  //   console.log(event, active);
  // }

  // public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
  //   console.log(event, active);
  // }

  numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
  }

  getRandomColor() {
    // var o = Math.round, r = Math.random, s = 255;
    // return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
      var result;
      var count = 0;
      for (var prop in this.baseColors)
          if (Math.random() < 2/++count)
             result = prop;
      
      return result;
  }

  clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

  reformCount(obj) {

    let countArr = [];

    Object.entries(obj).forEach(([key, val]) => {
      // console.log(key);
      // console.log(val);
      countArr.push({'date':key, 'count':val})
    });

    return countArr;
  }

  // getPageNum(party, page?) {
  //   this.getCommissionsByPartyList(party, page, this.shared.pageSize);
  //   console.log({'this.currPage':this.currPage});
  //   console.log({'this.lastPage':this.lastPage});
  //   if(this.currPage == this.lastPage) {
  //     this.nxt = true;
  //     this.prv = false;
  //   } else if(this.currPage == this.lastPage) {
  //     this.nxt = false;
  //     this.prv = true;
  //   }

  // }

  getCompanyInfo() {
    this.dsvc.getCompanyByID(localStorage.getItem('co_id')).subscribe(
      data => {
        // console.log({'data':data});
        this.company = data;
        // console.log({'company':this.company});
        this.shared.loader = false;
      }
    ),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

}
