import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ErrorComponent } from './error/error.component';
import { ViewComponent } from './packages/view/view.component';
import { ProtectedComponent } from './protected/protected.component';
import { AuthGuardService } from './services/auth-guard.service';

import { UsersComponent } from './users/users.component';
import { PackagesComponent } from './packages/packages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompaniesComponent } from './companies/companies.component';
import { PackageComponent } from './packages/package/package.component';
import { AgentsComponent } from './agents/agents.component';
import { AgentComponent } from './agents/agent/agent.component';
import { UserComponent } from './users/user/user.component';
import { ProfileComponent } from './profile/profile.component';
import { IncludesComponent } from './packages/includes/includes.component';
import { IncludeComponent } from './packages/includes/include/include.component';
import { BookingsComponent } from './bookings/bookings.component';
import { BookingComponent } from './bookings/booking/booking.component';
import { PaymentComponent } from './payments/payment/payment.component';
import { PaymentsComponent } from './payments/payments.component';
import { CommissionsComponent } from './commissions/commissions.component';
import { CommissionComponent } from './commissions/commission/commission.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { CommissionconfigsComponent } from './commissionconfigs/commissionconfigs.component';
import { CommissionconfigComponent } from './commissionconfigs/commissionconfig/commissionconfig.component';
import { ReportComponent } from './report/report.component';

export const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'view/:id',
    component: ViewComponent
  },
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent
  },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivateChild: [AuthGuardService],
    children:
    [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'report',
        component: ReportComponent
      },
      {
        path: 'agents',
        component: AgentsComponent
      },
      {
        path: 'agents/:id',
        component: AgentComponent
      },
      {
        path: 'agent',
        component: AgentComponent
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'users/:id',
        component: UserComponent
      },
      {
        path: 'company',
        component: CompaniesComponent
      },
      {
        path: 'company/:id',
        component: CompaniesComponent
      },
      {
        path: 'package',
        component: PackagesComponent
      },
      {
        path: 'package/:id',
        component: PackageComponent
      },
      {
        path: 'booking',
        component: BookingsComponent
      },
      {
        path: 'booking/:id',
        component: BookingComponent
      },
      {
        path: 'payment',
        component: PaymentsComponent
      },
      {
        path: 'payment/:id',
        component: PaymentComponent
      },
      {
        path: 'commissionconfig',
        component: CommissionconfigsComponent
      },
      {
        path: 'commissionconfig/:id',
        component: CommissionconfigComponent
      },
      {
        path: 'commission',
        component: CommissionsComponent
      },
      {
        path: 'commission/:id',
        component: CommissionComponent
      },
      {
        path: 'includes',
        component: IncludesComponent
      },
      {
        path: 'includes/:id',
        component: IncludeComponent
      },
      {
        path: 'resetpassword',
        component: ResetpasswordComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      }
    ]
  },
  {
    path: '**',
    component: ErrorComponent
  }
];

// @NgModule({
//   imports: [
//     RouterModule.forRoot(routes)
//   ],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
