import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { DataService } from '../services/data.service';
import { SharedService } from '../services/shared.service';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss'],
})
export class PackagesComponent implements OnInit, AfterViewInit {

  packages:any = [];
  totalElements: number;
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0;
  displayedColumns: string[];
  dataSource: MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  utype: any;

  constructor(
    private dsvc:DataService,
    public shared: SharedService) {
      this.shared.loader = true;
      this.shared.getPreviousUrl();
      // this.shared.checkSession();
      this.dataSource = new MatTableDataSource(this.packages);
    }

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
    
    ngOnInit() {
      this.utype = parseInt(localStorage.getItem('ut'));
      if(this.utype == 4)
        this.displayedColumns = ['num', 'code', 'name', 'travel_date', 'return_date', 'unit', 'action']
      else
        this.displayedColumns = ['num', 'code', 'name', 'travel_date', 'return_date', 'unit', 'status', 'action']
      this.getList(this.shared.pageNum,this.shared.pageSize);
      // this.getList(this.shared.pageNum,this.shared.pageSize);
    }

    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  getList(page,size) {

    let paginationUrl, urlPckg;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }

    if(this.utype == 4)
      urlPckg = 'package/active/list';
    else
      urlPckg = 'package';

    this.dsvc.getAll(urlPckg).subscribe(
      data => {
        this.packages = data;
        // this.packages = data['result']['data'];

        this.packages.forEach((item,index) =>  {
          item.row = index;

          item.daysNo = this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number');
          item.timeLeft = ' (in '+this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number')+' days)';
          item.travel_date = moment( item.travel_date).format('DD-MM-YYYY');
          item.return_date = moment(item.return_date).format('DD-MM-YYYY');
        });


        this.totalElements = this.packages.length;
        // console.log({'total':this.totalElements});
        this.dataSource.data = this.packages;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.shared.loader = false;
      }),
      error => {
        this.shared.loader = false;
        alert('Data unreachable!');
        console.log({'err':error});
    };
    this.shared.loader = false;
  }

  removeItem(id, name) {
    if(confirm("Delete: "+name+'?')) {
      this.shared.loader = true;
      this.dsvc.delPackage(id).subscribe(
        data => this.packages = data
      ),
      error => {
        this.shared.loader = false;
        alert('Data unreachable!');
        console.log({'err':error});
      };
      this.getList(this.shared.pageNum,this.shared.pageSize);
    } else {
    }
  }

}
