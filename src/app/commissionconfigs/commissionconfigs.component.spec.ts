import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionconfigsComponent } from './commissionconfigs.component';

describe('CommissionconfigsComponent', () => {
  let component: CommissionconfigsComponent;
  let fixture: ComponentFixture<CommissionconfigsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionconfigsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionconfigsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
