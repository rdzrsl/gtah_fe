import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { CommonService } from './common.service';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { SharedService } from './shared.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient, 
    private common:CommonService,
    public shared: SharedService
    ) { 

  }

  // createAuthorizationHeader(headers: Headers) {
  //   headers.append('Authorization', 'Bearer ' + localStorage.getItem('at')); 
  // }

  getPackages() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'package', { headers }).pipe(catchError(this.handleError));
  }

  getPackage(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // console.log({'headers':headers});
    
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'package/'+id, { headers, observe: "response" }).pipe(catchError(this.handleError));
  }

  getPackageByCompanyID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'package/co/'+id, { headers }).pipe(catchError(this.handleError));
  }

  getPackageByUserID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'package/user/'+id, { headers }).pipe(catchError(this.handleError));
  }

  addPackage(data) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.post(environment.backendURL+'package', data, { headers, observe: "response" });
  }

  putPackage(data: any, id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.put(environment.backendURL+'package/'+id, data, { headers, observe: "response" });
  }

  delPackage(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.delete(environment.backendURL+'package/'+id, { headers, observe: "response" });
  }

  getProducts() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'product', { headers }).pipe(catchError(this.handleError));
  }

  getProductByID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'product/'+id, { headers }).pipe(catchError(this.handleError));
  }

  getProductsByCompanyID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'product/co/'+id, { headers }).pipe(catchError(this.handleError));
  }

  addProduct(data) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.post(environment.backendURL+'product', data, { headers, observe: "response" });
  }

  putProduct(data, id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.put(environment.backendURL+'product/'+id, data, { headers, observe: "response" });
  }

  delProduct(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.delete(environment.backendURL+'product/'+id, { headers, observe: "response" });
  }

  getCompanyByID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'company/'+id, { headers }).pipe(catchError(this.handleError));
  }

  getCompanies() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'company', { headers }).pipe(catchError(this.handleError));
  }

  getUsers() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'user', { headers }).pipe(catchError(this.handleError));
  }

  getUserByID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'user/'+id, { headers }).pipe(catchError(this.handleError));
  }

  getUserTypes() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'usertypes', { headers }).pipe(catchError(this.handleError));
  }

  getUserTypeByID(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'usertypes/'+id, { headers }).pipe(catchError(this.handleError));
  }

  getUsersByType(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'user/type/'+id+'/'+localStorage.getItem('co_id'), { headers }).pipe(catchError(this.handleError));
  }

  addUser(data) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.post(environment.backendURL+'user', data, { headers, observe: "response" });
  }

  putUser(data, id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.put(environment.backendURL+'user/'+id, data, { headers, observe: "response" });
  }

  delUser(id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.delete(environment.backendURL+'user/'+id, { headers, observe: "response" }).pipe(catchError(this.handleError));
  }

  // GENERIC CRUD FUNCTIONS
  getAll(moduleName) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+moduleName, { headers }).pipe(catchError(this.handleError));
  }
  
  getAllByCoID(moduleName) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+moduleName+'/co/'+localStorage.getItem('co_id'), { headers }).pipe(catchError(this.handleError));
  }

  getByID(moduleName,id, nonlogin?) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    if(nonlogin) {
      headers = null;
    } else {
      headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    }
    return this.http.get(environment.backendURL+moduleName+'/'+id, { headers }).pipe(catchError(this.handleError));
  }

  store(moduleName, data) {
    
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.post(environment.backendURL+moduleName, data, { headers, observe: "response" }).pipe(catchError(this.handleError));
  }

  update(moduleName, data, id?): Observable<any> {
    let hasId;

    if(id)
      hasId = '/'+id;
    else
      hasId = '';

    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.put<any>(environment.backendURL+moduleName+hasId, data, { headers, observe: "response" }).pipe(catchError(this.handleError));
    // .pipe(
    //   map(model => {
    //     console.log(model.body);
    //     return model.body;
        
    //   }),
    //   catchError(this.handleError)
    //   );
  }

  delete(moduleName, id) {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.delete(environment.backendURL+moduleName+'/'+id, { headers, observe: "response" });
  }

  // UPLOADER
  uploadFiles(formData) {
    let headers = new HttpHeaders(); // ; boundary=" + Math.random().toString().substr(2)
    // headers.append('enctype',  'multipart/form-data');
    // headers.append('Content-Type',  'application/json');
    // headers.append('X-Requested-with',  'XMLHttpRequest');

    // let headers = new HttpHeaders({'Content-Type':  'application/json','Authorization': 'Bearer ' + localStorage.getItem('at')});
      // ); 
    // this.createAuthorizationHeader(headers);
    return this.http.post(environment.backendURL+'upload', formData, { observe: "response" });
  }

  // putPackage(formData: any, id) {
  //   let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
  //   // this.createAuthorizationHeader(headers);
  //   return this.http.put(environment.backendURL+'package/'+id, formData, { observe: "response" });
  // }
  

  public handleError(error: HttpErrorResponse) {

    let errObj = {
      'status': error.status,
      'statusText': error.statusText,
      'message': error.error.message,
    };
    // console.log(errObj);
    
    // return `${errObj.status} ${errObj.statusText} : ${errObj.message}`;
    // return errObj;
    // return throwError(JSON.stringify(errObj));
    return throwError(`${errObj.status} ${errObj.statusText} : ${errObj.message}`);
    // return throwError(errObj.status.toString() + ' ' + errObj.statusText + ' : ' +errObj.message);
  }

  // public handleError(error: HttpErrorResponse) {
  //   if (error.error instanceof ErrorEvent) {
  //     // A client-side or network error occurred. Handle it accordingly.
  //     console.error('An error occurred:', error.error.message);
  //   } else {
  //     // The backend returned an unsuccessful response code.
  //     // The response body may contain clues as to what went wrong,
  //     console.error(
  //       `Backend returned code ${error.status}, ` +
  //       `body was: ${error.error}`);
  //   }
  //   // return an observable with a user-facing error message
  //   return throwError(
  //     'Something bad happened; please try again later.');
  // };
}
