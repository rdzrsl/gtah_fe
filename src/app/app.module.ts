import {
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatCheckboxModule,
  MatSelectModule,
  MatDialogModule,
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatCardModule,
  MatChipsModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatSnackBarModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatNativeDateModule,
  MatSortModule,
  MatPaginatorModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { UsersComponent } from './users/users.component';
import { PostsComponent } from './posts/posts.component';
import { DetailsComponent } from './details/details.component';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { ToastrModule } from 'ngx-toastr';
import { CommonService } from './services/common.service';
import { ProductsComponent } from './products/products.component';
import { PackagesComponent } from './packages/packages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompaniesComponent } from './companies/companies.component';
import { PackageComponent, ViewPackageDialog } from './packages/package/package.component';
import { AgentsComponent } from './agents/agents.component';
import { AgentComponent } from './agents/agent/agent.component';
import { ErrorComponent } from './error/error.component';
import { ProductComponent } from './products/product/product.component';
import { UserComponent } from './users/user/user.component';
import { ProfileComponent } from './profile/profile.component';
import { IncludesComponent } from './packages/includes/includes.component';
import { IncludeComponent } from './packages/includes/include/include.component';
import { Ng4FilesModule } from './ng4-files';
import { ViewComponent, ViewDialog } from './packages/view/view.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RouterModule } from '@angular/router';
import { routes } from './app-routes';
import { ProtectedComponent } from './protected/protected.component';
import { BookingsComponent, ViewBookingRemarkDialog } from './bookings/bookings.component';
import { BookingComponent, ViewBookingDialog } from './bookings/booking/booking.component';
import { CustomersComponent } from './customers/customers.component';
import { CommissionsComponent, PaymentCommissionDialog } from './commissions/commissions.component';
import { CommissionComponent } from './commissions/commission/commission.component';
import { PaymentsComponent } from './payments/payments.component';
import { PaymentComponent } from './payments/payment/payment.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ChartsModule } from 'ng2-charts';
import { CommissionconfigsComponent } from './commissionconfigs/commissionconfigs.component';
import { CommissionconfigComponent } from './commissionconfigs/commissionconfig/commissionconfig.component';
import { ReportComponent } from './report/report.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    DetailsComponent,
    LoginComponent,
    ProductsComponent,
    PackagesComponent,
    DashboardComponent,
    CompaniesComponent,
    PackageComponent,
    AgentsComponent,
    AgentComponent,
    ErrorComponent,
    ProductComponent,
    UserComponent,
    ProfileComponent,
    IncludesComponent,
    IncludeComponent,
    ViewComponent,
    ProtectedComponent,
    SidebarComponent,
    ViewDialog,
    ViewBookingDialog,
    ViewPackageDialog,
    BookingComponent,
    BookingsComponent,
    CustomersComponent,
    CommissionsComponent,
    CommissionComponent,
    PaymentsComponent,
    PaymentComponent,
    PaymentCommissionDialog,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    ViewBookingRemarkDialog,
    CommissionconfigsComponent,
    CommissionconfigComponent,
    ReportComponent
  ],
  imports: [
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatSortModule,
    MatPaginatorModule,
    BrowserModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      closeButton: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 6000
    }),
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    Ng4FilesModule,
    RouterModule.forRoot(routes, { useHash: true }),
    ChartsModule
  ],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatSortModule,
    MatPaginatorModule,
    
    // LeftmenuComponent,
    // RightcontentComponent
],
  providers: [AuthService, CommonService, AuthGuardService],
  entryComponents: [ViewDialog,ViewBookingDialog, ViewPackageDialog, PaymentCommissionDialog, ViewBookingRemarkDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
