import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { SharedService } from '../services/shared.service';
import { CommonService } from 'src/app/services/common.service';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';

@Component({
  selector: 'app-commissionconfigs',
  templateUrl: './commissionconfigs.component.html',
  styleUrls: ['./commissionconfigs.component.scss']
})
export class CommissionconfigsComponent implements OnInit, AfterViewInit {

  commconfigs: any = [];
  totalElements: number;
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0;
  displayedColumns: string[] = ['num', 'name','total', 'status', 'action'];
  dataSource: MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private common:CommonService,
    private dsvc:DataService,
    public shared: SharedService
    ) {
      this.dataSource = new MatTableDataSource(this.commconfigs);
      this.shared.loader = true;
    }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    // this.getList(this.shared.pageNum,999);
    this.getList(this.shared.pageNum,this.shared.pageSize);
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getList(page,size) {

    let paginationUrl;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }
    this.dsvc.getAll('commconfig').subscribe(
      data => {

        this.commconfigs = data;
        // this.commconfigs = data['result']['data'];

        if(this.commconfigs.length != 0) {
          this.commconfigs.forEach(function(item,index)  {
            item.row = index;
          });

          this.totalElements = this.commconfigs.length;
          // console.log({'total':this.totalElements});
          this.dataSource.data = this.commconfigs;
          this.dataSource.paginator = this.paginator;
          this.paginator.length = this.totalElements;
          this.dataSource.sort = this.sort;
        }
        
        this.shared.loader = false;
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };
  }

  removeItem(id, name) {
    if(confirm("Delete: "+name+'?')) {
      this.dsvc.delete('commconfig', id).subscribe(
        data => {
          setTimeout(() => {
            this.common.showSuccess('Delete Successful!', '');
            this.getList(this.shared.pageNum,this.shared.pageSize);
          }, 1000);
      }),
      error => {
        this.common.showSuccess('Data unreachable!', '');
        console.log({'err':error});
      };
    } else {
    }
  }
}