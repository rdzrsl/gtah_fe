import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { SharedService } from '../services/shared.service';
import { DataService } from '../services/data.service';
import * as moment from 'moment-timezone';
import { Router, NavigationEnd } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  baseColors = {
    fuchsia: "#ff00ff",
    green: "#008000",
    lightgreen: "#90ee90",
    gold: "#ffd700",
    blue: "#0000ff",
    orange: "#ffa500",
    pink: "#ffc0cb",
    purple: "#800080",
    red: "#ff0000",
    silver: "#c0c0c0"
  };

  chart10colors: any = [
    { "color" : this.baseColors.blue },
    { "color" : this.baseColors.gold },
    { "color" : this.baseColors.green },
    { "color" : this.baseColors.lightgreen },
    { "color" : this.baseColors.fuchsia },
    { "color" : this.baseColors.orange },
    { "color" : this.baseColors.pink },
    { "color" : this.baseColors.purple },
    { "color" : this.baseColors.red },
    { "color" : this.baseColors.silver }
  ];

  monthsData: any = [
    { "name" : "January", "value" :1 },
    { "name" : "February", "value" :2 },
    { "name" : "March", "value" :3 },
    { "name" : "April", "value" :4 },
    { "name" : "May", "value" :5 },
    { "name" : "June", "value" :6 },
    { "name" : "July", "value" :7 },
    { "name" : "August", "value" :8 },
    { "name" : "September", "value" :9 },
    { "name" : "October", "value" :10 },
    { "name" : "November", "value" :11 },
    { "name" : "December", "value" :12 }
  ];

  public hbarChartType: ChartType = 'horizontalBar';

  public hbarChartData: ChartDataSets[] = [];
  
  public hbarChartLabels: Label[] = [];

  public dailyData = {data:[], fill: false, label: '-' };

  public monthlyData = {data:[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], fill: false, label: '-' };

  rptCurrMonth: any = [];
  
  public hbarChartOptions: (ChartOptions) = {
    responsive: true,
    scales: {
            xAxes: [{
              display: false,
              stacked: false,
              ticks: {
                stepSize: 1,
                min: 0,
                max: 10
              }
            }],
            yAxes: [{
                display: true,
                stacked: false,
                // ticks: {
                //   stepSize: 1,
                //   min: 0,
                //   max: 10
                // }
            },]
        },
        tooltips: {
          enabled: false,
          intersect: false
        },
        legend: {
          display: false
        }
  };
  previousTopAgents: any;

  public lineChartColors: Color[] = [
    {
      // borderColor: 'black',
      // backgroundColor: 'transparent',
    },
  ];

  currentMonth = moment().tz("Asia/Kuala_Lumpur").months()+1;
  previousMonth = moment().tz("Asia/Kuala_Lumpur").months();

  overallTotalSales:any = 0;
  currentMonthTotalSales:any = 0;
  previousMonthTotalSales:any = 0;
  userReport: any;
  currentMonthName: any;
  currentYear: any;
  selYear: any;
  selMonth: any;
  bookingStatus: any;
  bookings: any = [];
  yearVal: number;
  monthVal: any;
  bookingStatusVal: any;
  bookingStatusName: any;

  constructor(
    private router: Router,
    public shared: SharedService,
    private dsvc:DataService) {
      moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
      this.shared.loader = true;
    }

  ngOnInit() {

    // this.currentMonth = moment().month(this.currentMonth).format('m');
    // this.previousMonth = moment().month(this.previousMonth).format('m');
      // this.monthsData = this.monthsData.sort(function (a, b) { return a.value - b.value });
      this.currentMonthName = moment().tz("Asia/Kuala_Lumpur").format('MMMM');
      this.currentYear = moment().tz("Asia/Kuala_Lumpur").format('Y');
      this.selYear = this.currentYear;
      this.selMonth = this.previousMonth;
      this.bookingStatus = 'all';
      this.bookingStatusSel(this.bookingStatus);
    
    this.chart10colors.forEach(ce => {
      this.lineChartColors.push(
        {'backgroundColor':ce,
        'borderColor': ce}
        );
    });

    console.log({'selMonth':this.selMonth});
    
    this.genCurrentMonthDays();
    
    this.getReportByUser(1);
    this.yearVal = 2019;
    this.monthVal = this.selMonth;
    this.bookingStatusVal = 'all';
    this.getBookings();
  }

  genCurrentMonthDays() {
    // let currMonthTotalDays = moment().tz("Asia/Kuala_Lumpur").endOf("month").format('DD-MMM-YYYY');
    let currMonthTotalDays = moment().tz("Asia/Kuala_Lumpur").daysInMonth()+1;
    // console.log({'currMonthTotalDays':currMonthTotalDays});

    // this.dailyData.data.push(4);
    // this.dailyData.data.push(6);
    for(let i = 1; i < currMonthTotalDays; i++) {
      // this.lineChartLabels.push(moment().tz("Asia/Kuala_Lumpur").format(i+'-MMM-YYYY'));
      this.dailyData.data.push(0);
    }
    
    // this.lineChartData.push(this.dailyData);
    // console.log({'genCurrentMonthDays dailyData':this.dailyData});
    
    // this.getReportByCurrMonthSales();
    this.getCurrMonthReportByAgentSalesPerformance();
    
  }

  getCurrMonthReportByAgentSalesPerformance() {
    let temp:any = [];

    this.dsvc.getByID('booking/monthlyreport/'+localStorage.getItem('u_id'), 2).subscribe(
      data => {

        // console.log({'data':data});

        // this.previousTopAgents = data['prev'];

        // BAR CHART SEGMENT START
        data['prev'].slice(0,10).forEach((elBar, index) => {
          // console.log({'elBar':elBar}); 

          let hbarData = {
            'data': null,
            'label': null
          };

          this.hbarChartLabels.push(elBar.label);
          hbarData.label = elBar.label;
          hbarData.data = elBar.data;

          // console.log({'hbarData':hbarData});
          this.hbarChartData.push(hbarData);
        });
        // console.log({'hbarChartLabels':this.hbarChartLabels});
        // console.log({'hbarChartData':this.hbarChartData});

        this.hbarChartOptions = {
          responsive: true,
          scales: {
                  xAxes: [{
                    display: true,
                    stacked: false,
                    ticks: {
                      stepSize: 1,
                      min: 0,
                      // max: 10
                    }
                  }],
                  yAxes: [{
                    display: true,
                      stacked: false,
                      ticks: {
                          fontColor: '#000',
                          // Include a dollar sign in the ticks
                          // callback: function(value, index, values) {
                          //     return value;
                          // },
                          stepSize: 1,
                          min: 0
                      }
                  }]
              },
              tooltips: {
                enabled: false,
                intersect: false
              },
              legend: {
                display: false
              }
        };

        // this.maxValDaily = null;

        // BAR CHART SEGMENT END

        let tempDailyData = this.clone(this.dailyData.data);

        let tempDailyDataLabel = this.clone(this.dailyData.label);
        
        let tempDaily = { 'data': tempDailyData, fill: false, 'label': tempDailyDataLabel};

        // data['line'].slice(0,10).forEach((le,index) => {
        //   console.log({'le':le.name});
        
        // });
        //   console.log({'sliced':data['line'].slice(0,10).length});
        
        this.rptCurrMonth = data['line'].slice(0,10);
        
        // loop available agents
        this.rptCurrMonth.forEach(el => {
          // console.log({'el':el});

          let tempData = this.clone(tempDailyData);

            el.count = this.reformCount(el.count);

            this.dailyData.data = this.clone(tempData);
          
            // // loop available counts
            el.count.forEach(el1 => {
              // console.log({'el1':el1});

                let exist = Object.keys(this.dailyData.data).includes(el1.date);
                
                let ind = parseInt(el1.date);
                this.dailyData.label = el.label;
                // this.dailyData[ind].backgroundColor = this.getRandomColor();
                
                if(exist) {
                  this.dailyData.data[ind-1] = el1.count;
                } 
            });

            this.hbarChartData.forEach((el1, index) => {
              // console.log(index);
              // console.log(this.chart10colors[index].color);
              
              el1.backgroundColor = this.chart10colors[index].color;
              el1.borderColor = this.chart10colors[index].color;
            });
            
            this.dailyData = this.clone(tempDaily);
          });

          // this.lineChartData1.forEach((data) => this.dailyData.data.forEach((ddata) => {
          //   if(data > ddata)
          //     ddata = data;
          //   else if(data == ddata)
          //     data = ddata;
          // }));

          // console.log({'hbarChartData':this.hbarChartData});
          // console.log({'lineChartData1':this.lineChartData1});
          // console.log({'lineChartData1[0].data':this.lineChartData1[0].data});
          
        this.shared.loader = false;
      }
    ),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

  clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  }

  reformCount(obj) {

    let countArr = [];

    Object.entries(obj).forEach(([key, val]) => {
      // console.log(key);
      // console.log(val);
      countArr.push({'date':key, 'count':val})
    });

    return countArr;
  }

  getReportByUser(id) {
    
    this.dsvc.getByID('report/user',id).subscribe(
      data => {
        this.userReport = data;
        // console.log({'userReport data':data});

        if(this.userReport.overallSales)
          this.userReport.overallSales = this.numberWithCommas(this.userReport.overallSales);
        else
          this.userReport.overallSales = 0;

        if(this.userReport.overallCommission)
          this.userReport.overallCommission = this.numberWithCommas(this.userReport.overallCommission);
        else
          this.userReport.overallCommission = 0;

        if(this.userReport.currentMonthSales)
          this.userReport.currentMonthSales = this.numberWithCommas(this.userReport.currentMonthSales);
        else
          this.userReport.currentMonthSales = 0;

        if(this.userReport.currentMonthCommission)
          this.userReport.currentMonthCommission = this.numberWithCommas(this.userReport.currentMonthCommission);
        else
          this.userReport.currentMonthCommission = 0;


        this.shared.loader = false;
    },
    error => {
      this.shared.loader = false;
      console.log({'err':error.statusText});
    });

  }
  
  getTotalSales(month?) {

    let resTotalSales;

    this.shared.loader = true;

    this.dsvc.getAll('payment/sales/'+month).subscribe(
      data => {
        resTotalSales = data;
        console.log({'resTotalSales':resTotalSales});

        return resTotalSales;
      }),
      error => {
        this.shared.loader = false;
        console.log({'err':error});
      };
  }

  yearSel(e) {
    this.selYear = e;
  }

  monthSel(e) {
    this.selMonth = e;
  }

  bookingStatusSel(e) {
    this.bookingStatus = e;

    console.log({'bookingStatus':this.bookingStatus});
    if(e == 'all')
      this.bookingStatusName = 'All';
    else if(e == 1)
      this.bookingStatusName = 'Payment Ongoing';
    else if(e == 2)
      this.bookingStatusName = 'Paid Full';
    else if(e == 3)
      this.bookingStatusName = 'Cancelled';
  }

  submitBooking() {
    this.getBookings();
  }

  getBookings() {

    // this.selYear = 2019;
    // this.selMonth = 8;
    // this.bookingStatus = 2;

    this.shared.loader = true;

    this.dsvc.getAll('report/bookings/'+this.selYear+'/'+this.selMonth+'/'+this.bookingStatus).subscribe( // 
      data => {
        this.bookings = data;

        if(this.bookings.total != 0) {
          this.bookings.list.forEach((item,index) =>  {
            
            item.row = index;
            // item.timeLeftVal = this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number');
            // item.timeLeft = ' (in '+this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number')+' days)';
            // item.created_at = moment(item.created_at).format('DD-MM-YYYY h:mm A');
            item.travel_date = moment(item.travel_date).format('DD-MM-YYYY');

            // item.return_date = moment(item.return_date).format('DD-MM-YYYY');
          });
          // console.log({'bookings.total':this.bookings.total});
          // console.log({'bookings.list':this.bookings.list});

          // this.totalElements = this.bookings.length;
          // console.log({'total':this.totalElements});
          // this.dataSource.data = this.bookings;
          // this.dataSource.paginator = this.paginator;
          // this.dataSource.sort = this.sort;
      }
      this.shared.loader = false;
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };

    // if(this.shared.loader == true)
      // this.shared.loader = false;
  }

  printDiv(divName) {

    // var printContents = document.getElementById(divName);
    // .cloneNode(true);
    var printLabel = '<h3>Booking List: ' +moment().month(this.selMonth).format('MMMM')+ ' ' +this.selYear + ' (' + this.bookingStatusName + ')</h3>';
    var divToPrint = document.getElementById(divName);
    var newWin = window.open('', 'Print-Window');
    newWin.document.open();
    newWin.document.write('<html><head>' + document.head.innerHTML + '</head><body onload="window.print();setTimeout(function(){window.close();}, 100);">' + printLabel + divToPrint.innerHTML + '</body></html>'); newWin.document.close();
}

  numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
  }
}
