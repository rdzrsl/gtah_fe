import { Router, NavigationEnd} from '@angular/router';
import { Component } from '@angular/core';
import * as moment from 'moment-timezone';
import { SharedService } from './services/shared.service';
import { CommonService } from './services/common.service';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gtah';
  currentUrl: string;
  // moment = require('moment-timezone');

  constructor(private router:Router, public shared: SharedService, private common: CommonService, private dsvc: DataService) {
    router.events.subscribe(e => {
      // this.shared.loader = false;
      if (e instanceof NavigationEnd) {
        this.currentUrl = e.url;
        let currPage = this.currentUrl.split('/')[1];

        // if(currPage == 'view')
        //   this.shared.isView = true;
        // else
        //   this.shared.isView = false;
        
        moment.tz.setDefault("Asia/Kuala_Lumpur");
        moment().format('YYYY-MM-DD h:m A');
        this.getAllPackageIncludes();
        // console.log({'app date':moment().format('YYYY-MM-DD h:mm A')});
      }
    }),
    error => {
      alert('Error!');
      console.log({'err':error});
    };
   }

   getAllPackageIncludes() {
     
    this.dsvc.getAll('packageincludes/active')
    .subscribe(
      res => {
        this.shared.allPackageIncludesData = res;
        // console.log({'app shared.allPackageIncludesData':this.shared.allPackageIncludesData});
        
      }, err => {
        console.log({'err':err});
        this.shared.loader = false;
      });
      
   }
}
