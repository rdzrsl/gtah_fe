import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import * as moment from 'moment-timezone';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  username: FormControl;
  password: FormControl;
  body = {
    "email": null,
    "password": null
  };
  isLogin: boolean;
  name:string;
  // moment = require('moment-timezone');

  constructor(
    private auth:AuthService, 
    public common:CommonService,
    public shared: SharedService
  ) {
    this.isLogin = this.common.isLogin;
    this.shared.isView = true;
  
    moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
    //  this.shared.checkSession();;
    
   }

  ngOnInit() {
    // console.log(moment().format('YYYY-MM-DD hh:mm a'));
    
    this.isLogin = this.common.isLogin;
    
    if(localStorage.getItem('name')) {
      this.isLogin = true;
      this.name = localStorage.getItem('name');
    } else {
      this.isLogin = false;
      this.name = "";
    }

    this.username = new FormControl();
    this.password = new FormControl();

    this.form = new FormGroup({
      username: this.username,
      password: this.password,
    });
  }

  loginUser(formValues: any) {
    let plusword = this.makeid();
    let email2send;
    // console.log(formValues);

    this.body.password = 'toptal123';
    
    if(formValues.username == 'salman')
      email2send = 'gotoursandholiday@gmail.com';
    else if(formValues.username == 'aziman')
      email2send = 'aziman@yopmail.com';
    else if(formValues.username == 'redza')
      email2send = 'iamredza@gmail.com';
    else if(formValues.username == 'arafat')
      email2send = 'arafat@yopmail.com';
    else if(formValues.username == 'chombi')
      email2send = 'chombi@yopmail.com';
    else {
      email2send = formValues.username;
      this.body.password = formValues.password;
    }
    this.body.email = email2send;

    // this.body.password = 'toptal123';

    // this.form.get('password').setValue('toptal123');

    this.auth.getUserDetails(this.body);
  }

  userLogout() {
    this.auth.userLogout();
  }
  
  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 50; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }
  

}
