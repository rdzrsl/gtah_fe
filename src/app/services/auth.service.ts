import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from './common.service';
import * as moment from 'moment-timezone';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  at: any;
  co_id: any;
  u_id: any;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private common:CommonService, 
    // public shared: SharedService, 
    private router:Router, 
    private route:ActivatedRoute) {
      
  }

  validateUT(ut) {
    let res;

    let user = {
      "ut": ut
    }

    this.http.post(environment.backendURL+'validate', user).subscribe(
      data => {
        res = data;
        // console.log({'validated':res});
        if(res == false) {
          localStorage.removeItem('at');
          localStorage.removeItem('co_id');
          localStorage.removeItem('u_id');
          localStorage.removeItem('id');
          localStorage.removeItem('name');
          localStorage.removeItem('email');
          localStorage.removeItem('end');
    
          if(localStorage.getItem('ut'))
            localStorage.removeItem('ut');
    
          this.at = null;
          this.co_id = null;
          this.u_id = null;
          
          this.common.isLogin = false;
    
          this.router.navigate(['/']);
          // location.reload(true);
          window.location.href=window.location.href;
          this.toastr.error('Login Error/Expired: Please re-login!', '');
          console.log('reloaded');
        }
        
      },
      error => {
        this.toastr.error(error.status+': '+error.statusText, '');
        console.log({'error':error});
        
      }
    );
    return res;
  }

  validateEmail(email) {
    let res;

    let user = {
      "email": email
    }

    this.http.post(environment.backendURL+'validateEmail', user).subscribe(
      data => {
        res = data;
        
      },
      error => {
        this.toastr.error(error.status+': '+error.statusText, '');
        console.log({'error':error});
        
      }
    );
    return res;
  }

  getUserDetails(user) {
    
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Accept':  'application/json',
    //     'Content-Type':  'application/json',
    //     'Authorization': 'my-auth-token'
    //   })
    // };

    // this.http.post(this.common.beURL+'/api/login', user).subscribe(
    this.http.post(environment.backendURL+'login', user).subscribe(
      data => {
        let ate;
        
        this.toastr.success('Login Successful!', '');
        // this.shared.at = data['data']['api_token'];
        // this.shared.co_id = data['data']['co_id'];
        // this.shared.u_id = data['data']['id'];
        // this.shared.u_type = data['data']['type'];
        localStorage.setItem('ut', data['data']['type']);
        localStorage.setItem('at', data['data']['api_token']);
        localStorage.setItem('co_id', data['data']['co_id']);
        localStorage.setItem('u_id', data['data']['id']);
        localStorage.setItem('name', data['data']['name']);
        localStorage.setItem('email', data['data']['email']);
        
        ate = moment(data['data']['updated_at']).add(5,'minutes').format('YYYY-MM-DD hh:mm a');
        localStorage.setItem('end', ate);

        this.common.isLogin = true;
        // console.log(this.common.isLogin);
        // window.location.reload();

        this.router.navigate(['protected/dashboard']);
        
        // localStorage.setItem('end', data['updated_at']);
      },
      error => {
        if(error.error.errors.email) {
          this.toastr.error(error.error.errors.email,'');
          // console.log({'error':error.error.errors.email });
        } else if(error.error.errors.password) {
          this.toastr.error(error.error.errors.password, '');
          // console.log({'error':error.error.errors.password });
        }
        
      }
    );
  }

  userLogout() {

    let apitoken;

    if(localStorage.getItem('at'))
      apitoken = localStorage.getItem('at');
    else
      apitoken = "Unauthenticated";

    let token = {
      'api_token': apitoken
    }

    this.http.post(environment.backendURL+'logout', token).subscribe(
      data => {
        
        // console.log({'data':data});
        this.toastr.success('Logout Successful!', '');
        localStorage.removeItem('at');
        localStorage.removeItem('co_id');
        localStorage.removeItem('u_id');
        localStorage.removeItem('id');
        localStorage.removeItem('name');
        localStorage.removeItem('email');
        localStorage.removeItem('end');
        if(localStorage.getItem('ut'))
          localStorage.removeItem('ut');
        this.at = null;
        this.co_id = null;
        this.u_id = null;
        
        this.common.isLogin = false;
        // window.location.reload();

        this.router.navigate(['/']);

        // localStorage.setItem('end', data['updated_at']);
        this.toastr.success('Logout Successful!', '');
      },
      error => {
        this.toastr.error(error.status+': '+error.statusText, '');
        console.log({'error':error});
        
      }
    );

  }
  
}
