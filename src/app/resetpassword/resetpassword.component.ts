import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import * as moment from 'moment-timezone';
import { DataService } from '../services/data.service';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  form: FormGroup;
  username: FormControl;
  password: FormControl;
  password_confirmation: FormControl;
  body = {
    "email": null,
    "password": null,
    "password_confirmation": null,
  };
  loginEmail: string;

  constructor(
    private auth:AuthService, 
    public common:CommonService,
    private dsvc: DataService,
    public shared: SharedService
  ) {
    this.shared.loader = false;
    moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
  }

  ngOnInit() {
    if(localStorage.getItem('email'))
      this.loginEmail = localStorage.getItem('email');

      this.username = new FormControl('', [
        Validators.required,
        Validators.email,
      ]);
      this.password = new FormControl();
      this.password_confirmation = new FormControl();

    this.form = new FormGroup({
      username: this.username,
      password: this.password,
      password_confirmation: this.password_confirmation,
    });
      
    this.username.setValue(this.loginEmail);
    
  }

  validateUsername(email) {

    let emailBody = { "email": email };

    if(email && email.length > 0) {
      this.shared.loader = true;
      this.dsvc.store('validateEmail', emailBody).subscribe(res => {
        
        if(!res.body) {
          this.common.showError('Email does not exist in the system', '');
          // this.username.reset();
        }
          this.shared.loader = false;

      }),
      error => {
        this.shared.loader = false;
        console.log({ 'err': JSON.parse(error) });
        // this.toastr.error('Error! '+JSON.parse(error), '');
        this.common.showError(JSON.parse(error).errors, '');
      };
    }
  }

  submit(formValues: any) {

    this.body.email = formValues.username;
    this.body.password = formValues.password;
    this.body.password_confirmation = formValues.password_confirmation;

    // this.shared.loader = true;
    this.dsvc.store('resetpassword', this.body).subscribe(res => {
      // console.log({'res':res});
      // console.log({'res password reset':res.body});
        
      if(res.body['status'] == 500) {
        this.common.showError(res.body['message'], '');
        // this.username.reset();
      } else {
        this.common.showSuccess(res.body['message'], '');
        this.shared.loader = false;
        this.auth.userLogout();
      }
    }),
    error => {
      this.shared.loader = false;
      console.log({ 'error': error });
      // this.toastr.error('Error! '+JSON.parse(error), '');
      this.common.showError(JSON.parse(error).errors, '');
    };
  }

}
