import { Injectable, Inject, OnDestroy } from '@angular/core';
// import {Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  beURL:String = 'http://localhost:8000';
  isLogin:boolean = false;

  constructor(
    // public http: Http,
    private router: Router,
    private toastr: ToastrService) { }

    private toastSub: Subscription;
    private toastSub2: Subscription;

    numberOnly(event): boolean {
      const charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
  
    }
  
  showError(message, title) {

    // const controller = new AbortController();
    // $('.overlay-container').css('display', 'block');
    
    // let startHtml = '<div class="toast-modal-body"><div align="center" style="text-align: center;"><div class="row"><div class="col-sm-12" align="center"><h3 class="icon font-size-xl"><i class="fa fa-exclamation-triangle"></i></h2></div></div><div class="row"><div class="col-sm-12 msg"><h3 class="msg font-size-m" style="color: black;">';
    // let endHtml = '</h3></div></div></div></div>';
    // let toast = this.toastr.error(startHtml+message+endHtml, title);
    let toast = this.toastr.error(message, title);
    if (toast) {
      this.toastSub = toast.onHidden.subscribe(() => {
        // $('.overlay-container').css('display', 'none');
          this.toastSub.unsubscribe();
      }); 
    }
      
  }

  showSuccess(message, title) {
    // $('.overlay-container').css('display', 'block');
    
    // let startHtml = '<div class="toast-modal-body"><div align="center" style="text-align: center;"><div class="row"><div class="col-sm-12" align="center"><h3 class="icon font-size-xl"><i class="fa fa-check-circle"></i></h2></div></div><div class="row"><div class="col-sm-12 msg"><h3 class="msg font-size-m" style="color: black;">';
    // let endHtml = '</h3></div></div></div></div>';
    // let toast2 = this.toastr.success(startHtml+message+endHtml, title);
    let toast2 = this.toastr.success(message, title);
    if (toast2) {
      this.toastSub2 = toast2.onHidden.subscribe(() => {
        // $('.overlay-container').css('display', 'none');
          this.toastSub2.unsubscribe();
        });  
    } 
  }

  errorHandling(err, callback){
    // console.log({'err':err});
    
    if(err){
      if(err.status == 500){
          this.showError(err.body.message, '');
        } 
    } else {
      callback()
    }
  }
}
