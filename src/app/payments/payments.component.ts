import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { DataService } from '../services/data.service';
import { SharedService } from '../services/shared.service';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  payments:any = [];
  totalElements: number;
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0;
  displayedColumns: string[] = ['num', 'ref_no', 'contact_person', 'contact_no', 'total_paid', 'total_amt', 'balance', 'travel_date', 'created_at', 'action'];
  dataSource: MatTableDataSource<any>;
  agentID: any;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  userType: number;
  timeLeft: any;
  daysNo: any;

  constructor(
    private dsvc:DataService,
    private toastr: ToastrService,
    public shared: SharedService) {
      this.shared.getPreviousUrl();
      //  this.shared.checkSession();;
      this.dataSource = new MatTableDataSource(this.payments);
      this.shared.loader = true;
    }

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
      // console.log({'dataSource filter':this.dataSource.filter});
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
    
    ngOnInit() {
      this.agentID = parseInt(localStorage.getItem('u_id'));
      this.userType = parseInt(localStorage.getItem('ut'));
      this.getList(this.shared.pageNum,this.shared.pageSize);
    }

    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  getList(page,size) {

    let paginationUrl;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }

    this.dsvc.getAll('payment/user/'+this.agentID+'/'+this.userType).subscribe(
      data => {
        this.payments = data[0];

        if(this.payments.length != 0) {
          this.payments.forEach((item,index) =>  {
            item.row = index;
            item.balance = parseFloat(item.total_amt) - parseFloat(item.total_paid);
            item.balance = item.balance.toFixed(2);

            item.daysNo = this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number');
            item.timeLeft = ' (in '+this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number')+' days)';
            item.created_at = moment(item.created_at).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
            item.travel_date = moment(item.travel_date).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');
          });

          // console.log({'payments':this.payments});

          this.totalElements = this.payments.length;
          this.dataSource.data = this.payments;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
      }
      this.shared.loader = false;
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };
    this.shared.loader = false;
  }

  notify(obj) {
    this.shared.loader = true;
    // console.log({'obj':obj});

    let body = {
      "name":obj.contact_person,
      "email":obj.contact_email,
      "total":obj.total_amt,
      "paid":obj.total_paid,
      "balance":obj.balance,
      "created_at":obj.created_at,
      "travel_date":obj.travel_date,
      "ref_no": obj.code+' '+obj.ref_no,
      "daysno": obj.daysNo,
      "type": "notifymail"
    };

    this.dsvc.store('mailer', body).subscribe(
      res => {
        this.toastr.success(res.body['message'], '');
        this.shared.loader = false;
      }),
      error => {
        this.shared.loader = false;
        this.toastr.error('Error! '+error, '');
        console.log({'err':error});
      };
    
  }

  // removeItem(id, name) {
  //   if(confirm("Delete: "+name+'?')) {
  //     this.dsvc.delPackage(id).subscribe(
  //       data => this.payments = data
  //     ),
  //     error => {
  //       alert('Data unreachable!');
  //       console.log({'err':error});
  //     };
  //     this.getList(this.shared.pageNum,this.shared.pageSize);
  //   } else {
  //   }
  // }

}