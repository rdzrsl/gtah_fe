import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { DataService } from '../services/data.service';
import { SharedService } from '../services/shared.service';
import {MatPaginator, MatTableDataSource, MatSort, MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import * as moment from 'moment-timezone';

export interface DialogData {
  remark: any;
  refno: any;
}

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent implements OnInit {

  bookings:any = [];
  totalElements: number;
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0;
  displayedColumns: string[] = ['num', 'ref_no', 'contact_person', 'status', 'travel_date',  'created', 'action'];
  dataSource: MatTableDataSource<any>;
  userID: any;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  timeLeft: string;
  userType: number;

  constructor(
    public dialog: MatDialog,
    private dsvc:DataService,
    public shared: SharedService) {
      this.shared.getPreviousUrl();
      //  this.shared.checkSession();;
      this.dataSource = new MatTableDataSource(this.bookings);
      this.shared.loader = true;
    }

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
  
      if (this.dataSource.paginator) {
        this.dataSource.paginator.firstPage();
      }
    }
    
    ngOnInit() {
      this.userID = parseInt(localStorage.getItem('u_id'));
      this.userType = parseInt(localStorage.getItem('ut'));
      this.getList(this.shared.pageNum,this.shared.pageSize);
    }

    ngAfterViewInit() {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

  getList(page,size) {

    let paginationUrl;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }

    this.dsvc.getAll('booking/user/'+this.userID+'/'+this.userType).subscribe(
      data => {
        this.bookings = data;

        if(this.bookings.length != 0) {
          this.bookings.forEach((item,index) =>  {
            
            item.row = index;
            item.timeLeftVal = this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number');
            item.timeLeft = ' (in '+this.shared.timeLeftChecker(moment(item.travel_date).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number')+' days)';
            item.created_at = moment(item.created_at).format('DD-MM-YYYY h:mm A');
            item.travel_date = moment(item.travel_date).format('DD-MM-YYYY');

            // item.return_date = moment(item.return_date).format('DD-MM-YYYY');
          });


          this.totalElements = this.bookings.length;
          // console.log({'total':this.totalElements});
          this.dataSource.data = this.bookings;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
      }
      this.shared.loader = false;
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };

    // if(this.shared.loader == true)
      // this.shared.loader = false;
  }

  cancelItem(id, name) {
    if(confirm("Cancel booking: "+name+'?')) {
      this.shared.loader = true;
      this.dsvc.update('booking/cancel', '',id).subscribe(
        data => {
          // this.bookings = data;
          this.getList(this.shared.pageNum,this.shared.pageSize);
          this.shared.loader = false;
      }),
      error => {
        alert('Data unreachable!');
        console.log({'err':error});
        this.shared.loader = false;
      };
      // this.getList(this.shared.pageNum,this.shared.pageSize);
    } else {
    }
  }

  removeItem(id, name) {
    if(confirm("Delete: "+name+'?')) {
      this.shared.loader = true;
      this.dsvc.delPackage(id).subscribe(
        data => {
          this.bookings = data
          this.getList(this.shared.pageNum,this.shared.pageSize);
          this.shared.loader = false;
        }
      ),
      error => {
        alert('Data unreachable!');
        console.log({'err':error});
        this.shared.loader = false;
      };
    } else {
    }
  }

  openDialog(remark, refno): void {

    const dialogRef = this.dialog.open(ViewBookingRemarkDialog, {
      width: '800px',
      height: 'auto',
      data: {
        'remark':remark,
        'refno':refno
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}

@Component({
  selector: 'view-remark-dialog',
  templateUrl: './view-remark-dialog.html',
  styleUrls: ['./bookings.component.scss']
})
export class ViewBookingRemarkDialog {

  constructor(
    public dialogRef: MatDialogRef<ViewBookingRemarkDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}