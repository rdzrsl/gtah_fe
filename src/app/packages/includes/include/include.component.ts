import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-include',
  templateUrl: './include.component.html',
  styleUrls: ['./include.component.scss']
})
export class IncludeComponent implements OnInit {

  refId: any;
  getData: Object;
  includes: any = [];
  nullPointers: any = [];

  updateForm: FormGroup;

  name: FormControl; 
  description: FormControl; 
  include_id: any; 
  co_id: any; 
  active: FormControl;
  created_at: any; 
  updated_at: any;
  isActive: any;
  pageMode: string;
  complete: boolean;

  constructor(
    private router:Router,
    private dsvc:DataService,
    private toastr: ToastrService,
    public shared: SharedService
    ) {
      moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
      // this.shared.loader = true;
  }

  ngOnInit() {

    this.refId = this.router.url.split('/')[3];
    this.co_id = localStorage.getItem('co_id');

    this.name = new FormControl();
    this.description = new FormControl();
    // this.include_id = new FormControl();
    this.co_id = new FormControl(); 
    this.active = new FormControl();

    this.updateForm = new FormGroup ({
      name : this.name,
      description : this.description,
      co_id : this.co_id,
      active : this.active,
    });

    this.updateForm.get('active').setValue(0);

    if(this.refId === 'add') {
      this.pageMode = 'ADD';
    }else {
      this.getRow(this.refId);
      this.pageMode = 'EDIT';
    }
    this.checkReqValues();
  }

  getRow(id) {
    
    // Update ErrorMsg Service
    this.dsvc.getByID('packageincludes',id)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
        this.getData = Rdata;
      
        // console.log(this.getData);
        
        // populate data
        this.updateForm.get('name').setValue(this.getData['name']);
        this.updateForm.get('description').setValue(this.getData['description']);
        this.include_id = this.refId;

        if(localStorage.getItem('co_id') === '1')
          this.co_id = this.getData['co_id'];

        // this.updateForm.get('include_id').setValue(JSON.parse(this.getData['include_id']));
        this.updateForm.get('active').setValue((this.getData['active'] == 1)?true:false);

        this.isActive = this.getData['active'];
        this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
        this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

        this.checkReqValues();
        this.shared.loader = false;
      // }).bind(this));
    }, err => {
      this.shared.loader = false;
      console.log({'err':err});
    });
    
  }

  clear(elem) {
    this.updateForm.get(elem).setValue('');
  }

  back(module) {
    this.router.navigate([module])
  }

  checkReqValues() {

    let name = "name";
    // let include_id = "include_id";
    let active = "active";

    let reqVal: any = [name];
    let nullPointers: any = [];

    for (var reqData of reqVal) {
      let elem = this.updateForm.get(reqData);

      if (elem.value == "" || elem.value == null) {
        elem.setValue(null)
        nullPointers.push(null)
      }
    }

    if (nullPointers.length > 0) {
      this.complete = false;
    } else {
      this.complete = true;
    }

  }

  submit(formValues:any) {
    this.shared.loader = true;

    let body = {
      id: null,
      co_id: null,
      name: null,
      description: null,
      active: 0
    };

    // formValues.include_id = JSON.stringify(formValues.include_id);

    body.co_id = this.shared.co_id;
    if(this.refId !== 'add') {
      body.id = this.refId;
    
    body.name = formValues.name;
    body.description = formValues.description;
    body.active = formValues.active;
    
    // console.log({'edit body':body});
      this.dsvc.update('packageincludes', body, parseInt(this.refId))
      .subscribe(res => {
        console.log(res)
        this.toastr.success('Update Successful!', '');
        this.router.navigate(['protected/includes']);
        this.shared.loader = false;
      }),
      error => {
        console.log({'err':error.message});
        this.toastr.error('Error! '+error, '');
        this.shared.loader = false;
      };
    } else {
    
      body.name = formValues.name;
      body.description = formValues.description;
      body.active = formValues.active;

      // console.log({'add body':body});

      this.dsvc.store('packageincludes',body)
      .subscribe(res => {
        console.log(res)
        this.toastr.success('Add Successful!', '');
        this.router.navigate(['protected/includes']);
        this.shared.loader = false;
      }),
      error => {
        console.log({'err':JSON.parse(error.message)});
        this.toastr.error('Error! '+error, '');
        this.shared.loader = false;
      };

    }
  }

}
