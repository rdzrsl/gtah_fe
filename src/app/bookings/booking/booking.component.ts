import { Component, OnInit, ViewChild, Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';
import { MatDialog, MatVerticalStepper, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  Ng4FilesStatus,
  Ng4FilesSelected,
  Ng4FilesService,
  Ng4FilesConfig
} from './../../ng4-files';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { CommonService } from 'src/app/services/common.service';

export interface DialogData {
  src: string;
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit, OnDestroy {

  getPackageListSubs: Subscription;
  getPackageByCodeSubs: Subscription;
  getRowSubs: Subscription;
  getPayAmtSubs: Subscription;
  getMediaSubs: Subscription;
  removeAllMediaSubs: Subscription;
  removeAMediaSubs: Subscription;
  removefileConfigSubs: Subscription;
  timeoutSubs: any;

  isLinear = true;
  isUserDetailsCompleted = true;
  isPaxCompleted = true;
  isPaymentCompleted = true;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;

  refId: any;
  package_id: any;
  bookingId: any;
  getData: Object;
  nullPointers: any = [];
  events: string[] = [];
  packages: any = [];
  payments: any = [];

  packageCode: FormControl;
  participants: FormControl;
  contact_person: FormControl;
  contact_no: FormControl;
  contact_email: FormControl;
  contact_address: FormControl;
  paxno_adult: FormControl;
  paxno_childbed: FormControl;
  paxno_childnobed: FormControl;
  paxno_infant: FormControl;
  total_amt: FormControl;
  pay_amt: FormControl;
  remark: FormControl;
  paymentFile: FormControl;
  participantsFile: FormControl;
  created_at: any;
  updated_at: any;

  isActive: any;
  pageMode: string;
  complete: boolean;
  deposited: boolean;

  selectedPaymentFiles;
  selectedParticipantsFiles;
  selFiles = [];
  selParticipantsFiles = [];
  showImg: boolean;
  base64textString: string;
  media: any = [];
  bookingMedia: any = [];
  paymentMedia: any = [];

  filteredOptions: Observable<any[]>;
  adultPrice: any;
  childbedPrice: any;
  childnobedPrice: any;
  infantPrice: any;
  totalprice: any;
  deposit: any;
  paid_amt: any;
  balance_amt: any;
  agent_amt: any;
  hq_amt: any;
  etg_amt: any;

  @ViewChild(MatVerticalStepper) stepper: MatVerticalStepper;
  lastInsertId: any;
  lastInsertCode: any;
  paymentId: any;
  src2modal: string;
  travel_date: string;
  timeLeft: any;
  urlStorage = environment.storageURL;
  ref_no: any;
  ops_amt: number;
  agentCommissionId: any;
  payExceeded: boolean;
  balancePayable: any = 9999999;
  maxPax: any;
  totalSelPax: any = 0;
  bookingStatus: any;
  package_name: any;
  package_code: any;
  return_date: any;
  lastInsertId_booking: any;
  lastInsertId_commission: any;
  minPayAmt: any;

  totPrice1: any = 0;
  totPrice2: any = 0;
  totPrice3: any = 0;
  totPrice4: any = 0;
  payMessage: string;
  payAmtError: boolean;
  invoice_no: any;
  payFile: any = [];
  detailsFile: any = [];
  payment_id: any;
  commconfigs: any = [];
  commConfig: any = [];
  hq_comm: any;
  ops_comm: number;
  egt_comm: number;
  agent_comm: number;
  agent_id: any;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private dsvc: DataService,
    private toastr: ToastrService,
    public common: CommonService,
    private ng4FilesService: Ng4FilesService,
    public shared: SharedService
  ) {
    moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
    this.shared.loader = true;
    this.payExceeded = false;
    this.payAmtError = false;
    this.complete = false;
  }

  public fileConfig1: Ng4FilesConfig = {
    acceptExtensions: [
      'docx',
      'DOCX',
      'doc',
      'DOC',
      'pdf',
      'PDF',
      'png',
      'PNG',
      'jpg',
      'JPG',
      'jpeg',
      'JPEG'
    ],
    maxFilesCount: 5,
    maxFileSize: 5120000,
    totalFilesSize: 5120000
  };

  public fileConfig2: Ng4FilesConfig = {
    acceptExtensions: [
      'docx',
      'DOCX',
      'doc',
      'DOC',
      'pdf',
      'PDF',
      'png',
      'PNG',
      'jpg',
      'JPG',
      'jpeg',
      'JPEG'
    ],
    maxFilesCount: 5,
    maxFileSize: 5120000,
    totalFilesSize: 5120000
  };

  ngOnInit() {

    this.ng4FilesService.addConfig(this.fileConfig1, 'fileConfPay');
    this.ng4FilesService.addConfig(this.fileConfig2, 'fileConfPar');

    this.refId = this.router.url.split('/')[3];

    this.packageCode = new FormControl();
    this.participants = new FormControl();
    this.contact_person = new FormControl();
    this.contact_no = new FormControl();
    this.contact_address = new FormControl();
    this.contact_email = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);
    this.paxno_adult = new FormControl();
    this.paxno_childbed = new FormControl();
    this.paxno_childnobed = new FormControl();
    this.paxno_infant = new FormControl();
    this.total_amt = new FormControl();
    this.pay_amt = new FormControl();
    this.remark = new FormControl();
    // this.status = new FormControl(); 
    this.participantsFile = new FormControl();
    this.paymentFile = new FormControl();

    this.firstFormGroup = new FormGroup({
      contact_person: this.contact_person,
      contact_no: this.contact_no,
      contact_email: this.contact_email,
      contact_address: this.contact_address,
    });

    this.secondFormGroup = new FormGroup({
      packageCode: this.packageCode,
      paxno_adult: this.paxno_adult,
      paxno_childbed: this.paxno_childbed,
      paxno_childnobed: this.paxno_childnobed,
      paxno_infant: this.paxno_infant,
      total_amt: this.total_amt
    });

    this.thirdFormGroup = new FormGroup({
      pay_amt: this.pay_amt,
      paymentFile: this.paymentFile,
    });

    this.fourthFormGroup = new FormGroup({
      participants: this.participants,
      participantsFile: this.participantsFile,
      remark: this.remark,
    });

    this.getPackageList();
    this.getCommConfigList();

    this.paxno_adult.disable();
    this.paxno_childbed.disable();
    this.paxno_childnobed.disable();
    this.paxno_infant.disable();

    // this.thirdFormGroup.disable();
    // this.fourthFormGroup.disable();

    if (this.refId === 'add') {
      this.pageMode = 'ADD';
    } else {
      this.getRow(this.refId);
      this.isLinear = false;
    }
    this.checkReqValues();
  }

  ngOnDestroy() {
    if(this.timeoutSubs)
      clearInterval(this.timeoutSubs);

    if(this.getPackageListSubs)
      this.getPackageListSubs.unsubscribe();
      
    if (this.refId !== 'add') {

      if(this.getRowSubs)
        this.getRowSubs.unsubscribe();

      if(this.getMediaSubs)
        this.getMediaSubs.unsubscribe();
      
      if(this.getPackageByCodeSubs)
        this.getPackageByCodeSubs.unsubscribe();
      
      if(this.getPayAmtSubs)
        this.getPayAmtSubs.unsubscribe();
      // this.removeAllMediaSubs.unsubscribe();
    }
  }

  private _filter(value: any): any[] {
    let filterValue;

    if (value)
      filterValue = value.toLowerCase();
    else
      filterValue = value;

    return this.packages.filter(packages => packages.toLowerCase().indexOf(filterValue) === 0);
  }

  clear(elem, formType?) {
    if (elem == 'participantsFile') {
      this.fourthFormGroup.get(elem).reset();
      this.selParticipantsFiles = [];
      this.detailsFile = [];
      // this.removeAllImage(this.refId,elem);
    } else {
      this.getPackageList();
      if (formType && formType == 'firstFormGroup') {
        this.firstFormGroup.get(elem).reset();
      } else if (formType && formType == 'secondFormGroup') {
        this.secondFormGroup.get(elem).reset();
        if (elem == 'packageCode') {
          this.paxno_adult.reset();
          this.paxno_childbed.reset();
          this.paxno_childnobed.reset();
          this.paxno_infant.reset();
          this.total_amt.reset();
        }
      } else if (formType && formType == 'thirdFormGroup') {
        this.thirdFormGroup.get(elem).reset();
      } else {
        this.fourthFormGroup.get(elem).reset();
      }
      this.adultPrice = null;
      this.childbedPrice = null;
      this.childnobedPrice = null;
      this.infantPrice = null;
    }
    this.checkReqValues();
  }

  removeImage(id, name) {

    // console.log({'id':id});


    if (confirm("Delete: " + name + '?')) {
      this.removeAMediaSubs = this.dsvc.delete('media', id).subscribe(
      ),
        error => {
          alert('Data unreachable!');
        };
      this.getRow(this.refId);
      // this.getAllMedia(this.refId);
    } else {
    }
  }

  removeAllImage(id, elem?, formType?) {

    // console.log({'delete:':{'id':id, 'elem':elem, 'formType':formType}});

    if (confirm("Delete all files?")) {

      if (formType && formType == 'thirdFormGroup' && this.payFile.length != 0) {
        this.clear(elem, formType);
        this.payFile = [];
        this.selFiles = [];
        this.getAllMedia(this.refId + '/1', 'payment/booking');
      } else if (formType && formType == 'fourthFormGroup' && this.bookingMedia.length == 0) {
        this.clear(elem, formType);
      } else {
        this.removeAllMediaSubs = this.dsvc.delete('media/package', id).subscribe(
        ),
          error => {
            alert('Data unreachable!');
          };

        if (formType && formType == 'thirdFormGroup') {
          this.paymentMedia = [];
        } else if (formType && formType == 'fourthFormGroup') {
          this.bookingMedia = [];
        }
        this.secondFormGroup.get(elem).setValue('');
        this.getRow(this.refId);
        // this.getAllMedia(this.refId);
      }
      // else {
    }
    this.checkReqValues();
  }

  getRow(id) {

    this.shared.loader = true;

    // Update ErrorMsg Service
    this.getRowSubs = this.dsvc.getByID('booking', id)
      .subscribe(
        Rdata => {
          this.getData = Rdata;

          // populate data
          this.firstFormGroup.get('contact_person').setValue(this.getData['contact_person']);
          this.firstFormGroup.get('contact_no').setValue(this.getData['contact_no']);
          this.firstFormGroup.get('contact_email').setValue(this.getData['contact_email']);
          this.firstFormGroup.get('contact_address').setValue(this.getData['contact_address']);

          this.secondFormGroup.get('packageCode').setValue(this.getData['code']);
          this.setForm(this.getData['code']);
          this.secondFormGroup.get('paxno_adult').setValue(this.getData['paxno_adult']);
          this.secondFormGroup.get('paxno_childbed').setValue(this.getData['paxno_childbed']);
          this.secondFormGroup.get('paxno_childnobed').setValue(this.getData['paxno_childnobed']);
          this.secondFormGroup.get('paxno_infant').setValue(this.getData['paxno_infant']);
          this.deposit = this.getData['deposit'];
          this.totalprice = this.getData['total_amt'];
          this.secondFormGroup.get('total_amt').setValue(this.totalprice);

          this.ref_no = this.getData['ref_no'];
          this.agent_id = this.getData['agent_id'];

          this.getPaymentAmt(this.refId);
          this.getAllMedia(this.refId, 'booking');

          // this.thirdFormGroup.get('pay_amt').setValue(this.getData['contact_person']);
          // this.thirdFormGroup.get('paymentFile').setValue(this.getData['contact_no']);

          this.fourthFormGroup.get('remark').setValue(this.getData['remark']);
          // this.fourthFormGroup.get('contact_no').setValue(this.getData['contact_no']);
          // this.fourthFormGroup.get('contact_email').setValue(this.getData['contact_email']);
          // this.commonservice.errorHandling(Rdata, (function(){
          this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
          this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

          this.getData = Rdata;
          this.package_id = this.getData['package_id'];
          this.bookingStatus = this.getData['status'];
          this.invoice_no = this.getData['invoice_no'];

          this.checkReqValues();

          if (this.bookingStatus == 2 || this.bookingStatus == 3) {
            this.pageMode = 'VIEW';
            this.complete = true;
            this.thirdFormGroup.get('pay_amt').disable();
            // this.thirdFormGroup.get('paymentFile').disable();
            // this.fourthFormGroup.get('participantsFile').disable();
            // this.fourthFormGroup.get('remark').disable();
          } else {
            this.pageMode = 'EDIT';

            // this.checkReqValues();
          }
          // }).bind(this));
        }, err => {
          this.shared.loader = false;
        });

  }

  getPackageList() {

    this.shared.loader = true;
    let arr: any = [];
    this.packages = [];
    this.getPackageListSubs = this.dsvc.getAll('package/active/select').subscribe(
      data => {

        arr = data;

        arr.forEach(el => {
          if (el.unit != 0)
            this.packages.push(el.code);
        });

        // console.log({'packages':this.packages});

        this.filteredOptions = this.packageCode.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );
        this.shared.loader = false;

        // console.log({'packages':this.packages});

      }),
      error => {
        this.shared.loader = false;
        alert('Data unreachable!');
        // console.log({'err':error});
      };
  }

  getPackageByCode(code) {

    this.shared.loader = true;

    // Update ErrorMsg Service
    this.getPackageByCodeSubs = this.dsvc.getByID('package/view', code, true)
      .subscribe(
        Rdata => {
          // this.commonservice.errorHandling(Rdata, (function(){
          this.getData = Rdata[0];
          // console.log({'getData':this.getData});

          this.maxPax = this.getData['unit'];
          // console.log({'maxPax':this.maxPax});

          this.package_id = this.getData['id'];
          this.package_name = this.getData['name'];
          this.package_code = this.getData['code'];
          this.adultPrice = this.getData['adult_price'];
          this.childbedPrice = this.getData['childbed_price'] ? this.getData['childbed_price'] : 0.00;
          this.childnobedPrice = this.getData['childnobed_price'] ? this.getData['childnobed_price'] : 0.00;
          this.infantPrice = this.getData['infant_price'] ? this.getData['infant_price'] : 0.00;
          this.travel_date = moment(this.getData['travel_date']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');
          this.return_date = moment(this.getData['return_date']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');

          this.timeLeft = this.shared.timeLeftChecker(moment(this.getData['travel_date']).format('YYYY-MM-DD'), moment(new Date()).format('YYYY-MM-DD'), 'number');

          if (this.getData['cc_id']) {
            this.commConfig = this.getCommConfig(this.getData['cc_id']);
            this.hq_comm = parseInt(this.commConfig.hq)/100;
            this.agent_comm = parseInt(this.commConfig.agent)/100;
            this.egt_comm = parseInt(this.commConfig.consultant)/100;
            this.ops_comm = parseInt(this.commConfig.ops)/100;
            // let tempObj = [this.hq_comm, this.agent_comm, this.egt_comm, this.ops_comm];
            // console.log({'tempObj':tempObj});
            
          } else {

          }

          // this.secondFormGroup.get('paxno_adult').disable();
          if (!this.childbedPrice)
            this.secondFormGroup.get('paxno_childbed').disable();

          if (!this.childnobedPrice)
            this.secondFormGroup.get('paxno_childnobed').disable();

          if (!this.infantPrice)
            this.secondFormGroup.get('paxno_infant').disable();

          this.shared.loader = false;
          // }).bind(this));
        }, err => {
          // console.log({'err':err});
          this.shared.loader = false;
        });

  }

  back(module) {
    this.router.navigate([module])
  }

  setForm(code) {

    if (code != 0) {
      this.adultPrice = null;
      this.childbedPrice = null;
      this.childnobedPrice = null;
      this.infantPrice = null;

      this.paxno_adult.enable();
      this.paxno_childbed.enable();
      this.paxno_childnobed.enable();
      this.paxno_infant.enable();
      // console.log(code);
      this.getPackageByCode(code);
    } else {
      this.getPackageList();
      this.firstFormGroup.reset();
      this.secondFormGroup.reset();
      this.thirdFormGroup.reset();
      this.fourthFormGroup.reset();

      this.paxno_adult.disable();
      this.paxno_childbed.disable();
      this.paxno_childnobed.disable();
      this.paxno_infant.disable();
    }

  }

  getCommConfigList() {

    this.dsvc.getAll('commconfig/active').subscribe(
      data => {
        
        this.commconfigs = data;

    }),
    error => {
      alert('Data unreachable!');
      // console.log({'err':error});
    };
  }

  getCommConfig(id) {
    let res;

    this.commconfigs.forEach(el => {
      if(el.id == id)
        res = el;
    });
    return res;
  }

  checkMaxPaymentAmt(amt) {
    let res;

    // console.log({'amt length':amt.length});
    // console.log({'balance_amt':this.balance_amt});
    // if(amt.length <= this.balancePayable.length || amt.length == this.minPayAmt.length) {

      if (parseInt(amt) <= this.balancePayable && parseInt(amt) >= this.minPayAmt) {
        this.payExceeded = false;
        this.toastr.success('Payment Amount is acceptable.');
        this.payAmtError = false;
      } else if (parseInt(amt) <= this.balancePayable && parseInt(amt) < this.minPayAmt && this.pageMode == 'ADD') {
        this.payExceeded = false;
        this.toastr.error('Payment Amount is lower than deposit.');
        this.payAmtError = true;
        this.pay_amt.invalid;
      } 
      // else if (parseInt(amt) > this.balancePayable) {
      //   this.toastr.error('Payment Amount exceeded balance payable.');
      //   this.payExceeded = true;
      //   this.payAmtError = true;
      //   this.pay_amt.invalid;
      // } 
      else {
        this.pay_amt.valid;
        res = 0;
      }

      res = parseInt(amt);
      
      // console.log({'res':res});
      return res;
  }

  calculateTotal() {
    let tot1: any = 0;
    let tot2: any = 0;
    let tot3: any = 0;
    let tot4: any = 0;
    let totalPax = 0;
    this.totPrice1 = 0;
    this.totPrice2 = 0;
    this.totPrice3 = 0;
    this.totPrice4 = 0;

    if (!this.paxno_adult.value || this.paxno_adult.value == '') {
      tot1 = 0;
      this.paxno_adult.reset();
    } else {
      tot1 = parseInt(this.paxno_adult.value);
      this.totPrice1 = tot1 * parseFloat(this.adultPrice);
    }

    if (!this.paxno_childbed.value || this.paxno_childbed.value == '')
      tot2 = 0;
    else {
      tot2 = parseInt(this.paxno_childbed.value);
      this.totPrice2 = tot2 * parseFloat(this.childbedPrice);
    }

    if (!this.paxno_childnobed.value || this.paxno_childnobed.value == '')
      tot3 = 0;
    else {
      tot3 = parseInt(this.paxno_childnobed.value);
      this.totPrice3 = tot3 * parseFloat(this.childnobedPrice);
    }

    if (!this.paxno_infant.value || this.paxno_infant.value == '')
      tot4 = 0;
    else {
      tot4 = parseInt(this.paxno_infant.value);
      this.totPrice4 = tot4 * parseFloat(this.infantPrice);
    }

    totalPax = tot1 + tot2 + tot3 + tot4;
    // console.log({'totalPax':totalPax});

    this.totalSelPax = totalPax;
    // console.log({'totalSelPax':this.totalSelPax});

    if (totalPax > this.maxPax) {
      this.toastr.error('Maximum booking pax exceeded.', '');
    } else {

      // this.totalprice = 0;
      // this.total_amt.reset();

      this.totalprice = this.totPrice1 + this.totPrice2 + this.totPrice3 + this.totPrice4;
      // console.log({'totalprice':this.totalprice});

      this.secondFormGroup.get('total_amt').setValue(this.totalprice);

      // console.log({'total_amt':this.total_amt.value});
      // console.log('=========');
      let tempObj = [this.hq_comm, this.agent_comm, this.egt_comm, this.ops_comm];

      this.agent_amt = this.totalprice * this.agent_comm;
      this.hq_amt = this.totalprice * this.hq_comm;
      this.ops_amt = this.totalprice * this.ops_comm;
      this.etg_amt = this.totalprice * this.egt_comm;
    }
    // this.checkReqValues();
  }

  getPaymentAmt(id) {

    let p, t, b;

    this.shared.loader = true;
    // Update ErrorMsg Service
    this.getPayAmtSubs = this.dsvc.getByID('payment/booking/paid', id)
      .subscribe(
        Rdata => {

          // console.log({'Rdata':Rdata});

          // this.commonservice.errorHandling(Rdata, (function(){
          if (Rdata[0].length != 0) {

            this.payments = Rdata[0];

            this.payments.forEach(el => {
              el.updated_at = moment(el.updated_at).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
            });

            this.paymentId = Rdata[0][0].id;
            this.agentCommissionId = Rdata[0][0].commission_id;

            this.paid_amt = 'RM ' + Rdata[1];

            p = parseFloat(Rdata[1]);
            t = parseFloat(Rdata[0][0]['total_amt']);
            b = t - p;

            this.balancePayable = parseFloat(b);
            this.balance_amt = 'RM ' + b;

            // this.agent_amt = t * 0.13;
            // this.hq_amt = t * 0.13;
            // this.etg_amt = t * 0.02;

            this.getAllMedia(this.refId + '/1', 'payment/booking');
          }
          // }).bind(this));
          this.shared.loader = false;
        }, err => {
          this.shared.loader = false;
          // this.shared.loader = false;
        });
  }

  getAllMedia(id, moduleName?) {

    this.shared.loader = true;
    // Update ErrorMsg Service
    this.getMediaSubs = this.dsvc.getByID('media/' + moduleName, id)
      .subscribe(
        Rdata => {
          // this.commonservice.errorHandling(Rdata, (function(){
          // this.media = Rdata[0];

          // this.media.forEach(el => {
          //   arrFileNames.push(el.name);
          // });

          if (moduleName == 'payment/booking') {

            if(Rdata[0].length != 0) {
              this.paymentMedia = Rdata[0];
              // console.log({ 'this.paymentMedia': this.paymentMedia });
              this.payment_id = this.paymentMedia[0].payment_id;
              // console.log({ 'this.paymentMedia': this.payment_id });

              this.paymentMedia.forEach(el => {
                this.payFile.push(el.name);
              });
              // this.thirdFormGroup.controls.paymentFile.setValue(this.payFile.join(", "));
            }
          }

          if (moduleName == 'booking') {

            if(Rdata[0].length != 0) {
              this.bookingMedia = Rdata[0];
              // console.log({'this.bookingMedia':this.bookingMedia});
              this.bookingMedia.forEach(el => {
                this.payFile.push(el.name);
              });
              this.fourthFormGroup.controls.participantsFile.setValue(this.detailsFile.join(", "));
            }
          }

          // }).bind(this));
          this.shared.loader = false;
        }, err => {
          this.shared.loader = false;
          // this.shared.loader = false;
        });
  }

  paymentFileSelect(selectedPaymentFiles: Ng4FilesSelected): void {

    let mFileSize = this.fileConfig1.maxFileSize;

    // let fileExtn = selectedPaymentFiles.files[0].name.split('.')[1];/
    // let chkFileExtn = this.resFileExtn.filter(fData => fData === fileExtn.toLowerCase());

    if (selectedPaymentFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
      if (selectedPaymentFiles.files.length > 0 && mFileSize) {
        // if (selectedPaymentFiles.files[0].size <= mFileSize) {
        // Check File extn  
        // if (chkFileExtn.length > 0){
        // this.filesResult.en.size = selectedPaymentFiles.files[0].size;

        // this.selectedPaymentFiles = selectedPaymentFiles.files[0].name;
        let file2convert;
        var file: File;
        let extension = null;
        let mediaType = null;

        selectedPaymentFiles.files.forEach(el => {

          this.payFile.push(el.name);
          extension = el.name.split('.').pop();
          mediaType = this.getFileCode(extension);

          file = el;
          let myReader: FileReader = new FileReader();

          myReader.onloadend = (e) => {
            file2convert = myReader.result;
            this.selFiles.push({ "path": null, "name": el.name, "size": el.size, "type": el.type, "ext": extension, "code": mediaType, "src": file2convert });
          }
          myReader.readAsDataURL(file);
        });

        // this.selFiles = tempArr;
        // console.log({ 'selFiles': this.selFiles });



        this.thirdFormGroup.controls.paymentFile.setValue(this.payFile.join(", "));

        // if (selectedPaymentFiles.files) {
        // }
        this.showImg = true;
        //  }else {
        //   this.commonservice.showError('File Extension not match','');
        //  }    
      } else {
        this.toastr.error('File Size Exceed maximum file size', '');
      }

    } else if (selectedPaymentFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
      this.toastr.error('Maximum files count exceed.Please upload one file', '');
    } else if (selectedPaymentFiles.status === Ng4FilesStatus.STATUS_MAX_FILE_SIZE_EXCEED) {
      this.toastr.error('Maximum files size exceed', '');
    }

    this.selectedPaymentFiles = Array.from(selectedPaymentFiles.files).map(file => file.name);

    this.checkReqValues();
  }

  participantFilesSelect(selectedParticipantsFiles: Ng4FilesSelected): void {

    let mFileSize = this.fileConfig2.maxFileSize;

    // let fileExtn = selectedParticipantsFiles.files[0].name.split('.')[1];
    // let chkFileExtn = this.resFileExtn.filter(fData => fData === fileExtn.toLowerCase());

    if (selectedParticipantsFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
      if (selectedParticipantsFiles.files.length > 0 && mFileSize) {
        // if (selectedParticipantsFiles.files[0].size <= mFileSize) {
        // Check File extn  
        // if (chkFileExtn.length > 0){
        // this.filesResult.en.size = selectedParticipantsFiles.files[0].size;

        // this.selectedParticipantsFiles = selectedParticipantsFiles.files[0].name;
        this.selParticipantsFiles = [];
        let file2convert;
        var file: File;
        let extension = null;
        let mediaType = null;

        selectedParticipantsFiles.files.forEach(el => {

          this.detailsFile.push(el.name);
          extension = el.name.split('.').pop();
          mediaType = this.getFileCode(extension);

          file = el;
          let myReader: FileReader = new FileReader();

          myReader.onloadend = (e) => {
            file2convert = myReader.result;
            this.selParticipantsFiles.push({ "path": null, "name": el.name, "size": el.size, "type": el.type, "ext": extension, "code": mediaType, "src": file2convert });
          }
          myReader.readAsDataURL(file);
        });
        // this.selParticipantsFiles = this.selParticipantsFiles;
        // console.log({'selParticipantsFiles':this.selParticipantsFiles});

        this.fourthFormGroup.controls.participantsFile.setValue(this.detailsFile.join(", "));

        // if (selectedParticipantsFiles.files) {
        // }
        this.showImg = true;
      } else {
        this.toastr.error('File Size Exceed maximum file size', '');
      }

    } else if (selectedParticipantsFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
      this.toastr.error('Maximum files count exceed.Please upload one file', '');
    } else if (selectedParticipantsFiles.status === Ng4FilesStatus.STATUS_MAX_FILE_SIZE_EXCEED) {
      this.toastr.error('Maximum files size exceed', '');
    }

    this.selectedParticipantsFiles = Array.from(selectedParticipantsFiles.files).map(file => file.name);
  }

  goTo(fileSrc) {
    let fullSrc = this.urlStorage + fileSrc;

    console.log(fullSrc);

    window.open(fullSrc, '_blank');
  }

  getFileCode(ext) {
    let exist, res;

    exist = this.fileConfig1.acceptExtensions.includes(ext.toLowerCase());

    res = exist;

    if (exist) {
      switch (ext) {
        case 'docx':
          res = 'word';
          break;

        case 'doc':
          res = 'word';
          break;

        case 'pdf':
          res = 'pdf';
          break;

        case 'png':
          res = 'image';
          break;

        case 'jpg':
          res = 'image';
          break;

        case 'jpeg':
          res = 'image';
          break;

        default:
          break;
      }
    }

    return res;
  }

  checkReqValues() {

    let reqVal1: any = [
      'contact_person',
      'contact_no',
      'contact_email',
      'contact_address'
    ];
    let reqVal2: any = [
      // 'packageCode',
      'paxno_adult'
    ];
    let reqVal3: any = [
      'pay_amt',
      'paymentFile'
    ];
    let reqVal4: any = [
      'participantsFile'
    ];

    let nullPointers: any = [];

    if (this.pageMode != 'VIEW') {

      for (var reqData1 of reqVal1) {
        let elem = this.firstFormGroup.get(reqData1);
  
        if (elem.value == "" || elem.value == null) {
          elem.setValue(null);
          nullPointers.push(null);
        }
      }

      for (var reqData2 of reqVal2) {
        let elem = this.secondFormGroup.get(reqData2);
  
        if (elem.value == "" || elem.value == null) {
          elem.setValue(null);
          nullPointers.push(null);
        }
      }

      for (var reqData3 of reqVal3) {
        let elem = this.thirdFormGroup.get(reqData3);

        if (elem.value == "" || elem.value == null) {
          elem.setValue(null)
          nullPointers.push(null)
        }
      }

      // if (this.totalSelPax > this.maxPax) { 
      //   this.isPaxCompleted = false;
      // } else {
      //   this.isPaxCompleted = true;
      // }

      // if (this.bookingStatus == 2 || this.bookingStatus == 3)
      //   this.complete = false;

      // if (this.payAmtError)
      //   this.isPaymentCompleted = false;
      
      if (this.pay_amt.value && this.pay_amt.value.length < 2) {
        this.payAmtError = true;
      } else {
        this.payAmtError = false;
      }
    }

    if(this.totalSelPax > this.maxPax) {
      nullPointers.push(null);
      this.toastr.error('Maximum booking pax exceeded.', '');
    } else if(this.totalSelPax <= this.maxPax && nullPointers.length == 0) {
      nullPointers = [];
    }
    
    if (nullPointers.length > 0) {
      this.complete = false;
    } else {
      this.complete = true;
    }


    // console.log({'paxExceeded':paxExceeded});
    // console.log({'payAmtError':this.payAmtError});
    // console.log({'nullPointers.length':nullPointers.length});
  }

  paymentAmountConfirmation(){
    if(this.pay_amt.value) {
      if(confirm("Proceed with the payment amount of: RM "+this.pay_amt.value+'?')) {
        // this.getList(this.shared.pageNum,this.shared.pageSize);
      } else {
        this.pay_amt.reset();
      }
      this.checkReqValues();
    }
  }

  submit() {

    this.shared.loader = true;

    let body = {
      id: null,
      ref_no: null,
      agent_id: null,
      package_id: null,
      code: null,
      contact_person: null,
      contact_no: null,
      contact_email: null,
      contact_address: null,
      paxno_adult: 0,
      paxno_childbed: 0,
      paxno_childnobed: 0,
      paxno_infant: 0,
      total_amt: null,
      remark: null,
      uploadtype: 'booking',
      files: null
    };

    let paymentBody = {
      id: null,
      commission_id: null,
      booking_id: null,
      paid_amt: null,
      type: 1,
      total_amt: null,
      uploadtype: 'payment',
      files: null
    };

    let commissionBody = {
      "data": [
        {
          id: null,
          booking_id: null,
          user_id: null,
          amount: null
        },
        {
          id: null,
          booking_id: null,
          user_id: 2,
          amount: null
        },
        {
          id: null,
          booking_id: null,
          user_id: 26,
          amount: null
        },
        {
          id: null,
          booking_id: null,
          user_id: 1,
          amount: null
        }
      ]
    };

    let receiptContent = {
      invoice_no: null,
      receipt_no: null,
      contact_person: null,
      contact_email: null,
      contact_address: null,
      type: "receiptmail"
    };

    if (this.selFiles.length == 0 || this.selFiles == [])
      delete paymentBody['files'];

    if (this.selParticipantsFiles.length == 0 || this.selParticipantsFiles == [])
      delete body['files'];

    if (this.refId !== 'add') { // EDIT & VIEW

      body.id = this.refId;
      body.ref_no = this.ref_no;
      body.agent_id = this.agent_id;
      body.package_id = this.package_id;
      body.code = this.packageCode.value;
      body.contact_person = this.contact_person.value;
      body.contact_no = this.contact_no.value;
      body.contact_email = this.contact_email.value;
      body.contact_address = this.contact_address.value;
      body.paxno_adult = parseInt(this.paxno_adult.value);
      body.paxno_childbed = parseInt(this.paxno_childbed.value);
      body.paxno_childnobed = parseInt(this.paxno_childnobed.value);
      body.paxno_infant = parseInt(this.paxno_infant.value);
      body.total_amt = parseFloat(this.total_amt.value);
      body.remark = this.remark.value;
      body.files = this.selParticipantsFiles;

      paymentBody.booking_id = parseInt(this.refId);
      paymentBody.commission_id = this.agentCommissionId;
      paymentBody.total_amt = parseFloat(this.total_amt.value);
      paymentBody.files = this.selFiles;

      receiptContent.invoice_no = this.invoice_no;

      receiptContent.contact_person = this.contact_person.value;
      receiptContent.contact_email = this.contact_email.value;
      receiptContent.contact_address = this.contact_address.value;

      // console.log({'selFiles':this.selFiles});
      // console.log({'selParticipantsFiles':this.selParticipantsFiles});

      if (this.pageMode == 'VIEW') {
        paymentBody.id = this.payment_id;
        paymentBody.paid_amt = null;
      } else {
        paymentBody.paid_amt = parseFloat(this.pay_amt.value);
      }
      // console.log({'body':body});
      // console.log({'paymentBody':paymentBody});

      this.dsvc.update('booking/' + this.refId, body)
        .subscribe(res => {

          // console.log({'res':res});

          // console.log({'pay_amt.value':this.pay_amt.value});

          if (this.pay_amt.value != null && this.pageMode != 'VIEW') {

            // PAYMENT
            this.dsvc.store('payment', paymentBody)
              .subscribe(res => {

                paymentBody.id = res.body['last_insert_id'];
                receiptContent.receipt_no = res.body['receipt_no'];

                let receiptBody = Object.assign(paymentBody, receiptContent);

                if (this.selFiles.length != 0) { // Payment
                  this.dsvc.store('media', paymentBody).subscribe(
                    res => {
                      this.toastr.success('Payment file(s) upload successful!', '');
                      // this.shared.loader = false;
                    }),
                    error => {
                      this.shared.loader = false;
                      this.toastr.error('Error! ' + error, '');
                      console.log({ 'err': error });
                    };
                }

                this.dsvc.store('mailer', receiptBody).subscribe(
                  res => {
                    this.toastr.success(res.body['message'], '');
                    // this.shared.loader = false;
                  }),
                  error => {
                    this.shared.loader = false;
                    this.toastr.error('Error! ' + error, '');
                    console.log({ 'err': error });
                  };

                this.toastr.success('Payment Update Successful!', '');

                // this.shared.loader = false;
              },
                error => {
                  this.toastr.error('Error! ' + error, '');
                  this.shared.loader = false;

                });
          } else {

            if (this.selFiles.length != 0) { // Payment
              this.dsvc.store('media', paymentBody).subscribe(
                res => {
                  this.toastr.success('Payment file(s) upload successful!', '');
                  // this.shared.loader = false;
                }),
                error => {
                  this.shared.loader = false;
                  this.toastr.error('Error! ' + error, '');
                  console.log({ 'err': error });
                };
            } 
            if (this.selParticipantsFiles.length != 0) { // Participants Details

              this.dsvc.store('media', body).subscribe(
                res => {
                  this.toastr.success('Participants Details file(s) upload successful!', '');
                  // this.shared.loader = false;
                }),
                error => {
                  this.shared.loader = false;
                  this.toastr.error('Error! ' + error, '');
                  console.log({ 'err': error });
                };
            }
          }

          this.timeoutSubs = setTimeout(() => {
            this.toastr.success('Booking Update Successful!', '');
            this.router.navigate(['protected/booking']);
            this.shared.loader = false;
          }, 10000);

        }),
        error => {
          this.shared.loader = false;
          this.toastr.error('Error! ' + error, '');
          console.log({ 'err': error });
        };

    } else { // ADD

      let invoiceContent = {
        invoice_no: null,
        package_name: this.package_name,
        package_code: this.package_code,
        adultPrice: parseFloat(this.adultPrice),
        childbedPrice: parseFloat(this.childbedPrice),
        childnobedPrice: parseFloat(this.childnobedPrice),
        infantPrice: parseFloat(this.infantPrice),
        adult_amount: this.totPrice1,
        childbed_amount: this.totPrice2,
        childnobed_amount: this.totPrice3,
        infant_amount: this.totPrice4,
        travel_date: this.travel_date,
        return_date: this.return_date,
        type: "ordermail"
      };

      body.agent_id = parseInt(localStorage.getItem('u_id'));
      body.package_id = this.package_id;
      body.code = this.packageCode.value;
      body.contact_person = this.contact_person.value;
      body.contact_no = this.contact_no.value;
      body.contact_email = this.contact_email.value;
      body.contact_address = this.contact_address.value;
      body.paxno_adult = parseInt(this.paxno_adult.value);
      body.paxno_childbed = (this.paxno_childbed.value) ? parseInt(this.paxno_childbed.value) : 0;
      body.paxno_childnobed = (this.paxno_childnobed.value) ? parseInt(this.paxno_childnobed.value) : 0;
      body.paxno_infant = (this.paxno_infant.value) ? parseInt(this.paxno_infant.value) : 0;
      body.total_amt = parseFloat(this.total_amt.value);
      body.remark = this.remark.value;
      body.files = this.selParticipantsFiles;

      paymentBody.paid_amt = parseFloat(this.pay_amt.value);
      paymentBody.total_amt = parseFloat(this.total_amt.value);
      paymentBody.files = this.selFiles;

      commissionBody.data[0].user_id = parseInt(localStorage.getItem('u_id'));
      commissionBody.data[0].amount = parseFloat(this.agent_amt.toFixed(2));
      commissionBody.data[1].amount = parseFloat(this.hq_amt.toFixed(2));
      commissionBody.data[3].amount = parseFloat(this.etg_amt.toFixed(2));
      commissionBody.data[2].amount = parseFloat(this.ops_amt.toFixed(2));

      receiptContent.contact_person = this.contact_person.value;
      receiptContent.contact_email = this.contact_email.value;
      receiptContent.contact_address = this.contact_address.value;

      // console.log({'body':body});
      // console.log({'paymentBody':paymentBody});
      // console.log({'commissionBody':commissionBody});

      // BOOKING
      this.dsvc.store('booking', body)
        .subscribe(res => {

          // console.log({ 'res': res });
          if(res.body['full']) {
            this.toastr.error(res.body['message'], '', {
              timeOut: 5000
            });
            this.stepper.selectedIndex = 1;
            this.secondFormGroup.reset();
            this.totalSelPax = 0;

            this.getPackageList();
            // this.thirdFormGroup.reset();
            // this.fourthFormGroup.reset();
          } else {

          this.lastInsertId_booking = res.body[1][0];
          invoiceContent.invoice_no = res.body[1][1];
          receiptContent.invoice_no = res.body[1][1];
          body.ref_no = res.body[1][2];
          body.id = this.lastInsertId_booking;

          let invoiceBody = Object.assign(body, invoiceContent);

          // console.log({ 'invoiceBody': invoiceBody });

          if (this.selParticipantsFiles.length != 0) { // Participants Details

            this.dsvc.store('media', body).subscribe(
              res => {
                this.toastr.success('Participants Details file(s) upload successful!', '');
                // this.shared.loader = false;
              }),
              error => {
                this.shared.loader = false;
                this.toastr.error('Error! ' + error, '');
                console.log({ 'err': error });
              };
          }

          this.dsvc.store('mailer', invoiceBody).subscribe(
            res => {
              this.toastr.success(res.body['message'], '');
              // this.shared.loader = false;
            }),
            error => { // MAILER
              this.shared.loader = false;
              this.toastr.error('Error! ' + error, '');
              console.log({ 'err': error });
            };

          paymentBody.booking_id = this.lastInsertId_booking;
          commissionBody.data[0].booking_id = this.lastInsertId_booking;
          commissionBody.data[1].booking_id = this.lastInsertId_booking;
          commissionBody.data[2].booking_id = this.lastInsertId_booking;
          commissionBody.data[3].booking_id = this.lastInsertId_booking;

          // COMMISSION
          this.dsvc.store('commission', commissionBody)
            .subscribe(res => {

              this.lastInsertId_commission = res.body['last_insert_id'];

              paymentBody.commission_id = this.lastInsertId_commission;
              this.toastr.success('Commission Add Successful!', '');
              // this.shared.loader = false;

              // PAYMENT
              this.dsvc.store('payment', paymentBody)
                .subscribe(res => {

                  this.toastr.success('Payment Add Successful!', '');

                  paymentBody.id = res.body['last_insert_id'];
                  receiptContent.receipt_no = res.body['receipt_no'];

                  let receiptBody = Object.assign(paymentBody, receiptContent);

                  if (this.selFiles.length != 0) { // Payment
                    this.dsvc.store('media', paymentBody).subscribe(
                      res => {
                        this.toastr.success('Payment file(s) upload successful!', '');
                        // this.shared.loader = false;
                      }),
                      error => { // PAYMENT UPLOAD
                        this.shared.loader = false;
                        this.toastr.error('Error! ' + error, '');
                        console.log({ 'err': error });
                      };
                  }

                  this.dsvc.store('mailer', receiptBody).subscribe(
                    res => {
                      this.toastr.success(res.body['message'], '');
                      // this.shared.loader = false;
                    }),
                    error => { // MAILER
                      this.shared.loader = false;
                      this.toastr.error('Error! ' + error, '');
                      console.log({ 'err': error });
                    };

                  // this.shared.loader = false;

                },
                  error => { // PAYMENT
                    this.toastr.error('Error! ' + error, '');
                    this.shared.loader = false;

                  });
            },
            error => { // COMMISSION
              this.toastr.error('Error! ' + error, '');
              this.shared.loader = false;

            });
          }
        },
        error => { // BOOKING
          console.log({ 'error': error });
          this.toastr.error(error, '');
          this.shared.loader = false;
        });

        this.timeoutSubs = setTimeout(() => {
          this.toastr.success('Booking Add Successful!', '');
          this.router.navigate(['protected/booking']);
          this.shared.loader = false;
        }, 10000);
    }
  }

  openDialog(imgSrc, refid, folderName?): void {

    if (folderName == 'payment')
      this.src2modal = '/payment/' + refid + '/' + imgSrc;
    else if (folderName == 'booking')
      this.src2modal = '/booking/' + refid + '/' + imgSrc;

    const dialogRef = this.dialog.open(ViewBookingDialog, {
      width: '800px',
      height: '600px',
      data: { 'src': this.urlStorage + this.src2modal }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}

@Component({
  selector: 'view-dialog',
  templateUrl: './view-dialog.html',
  styleUrls: ['./booking.component.scss']
})
export class ViewBookingDialog {

  constructor(
    public dialogRef: MatDialogRef<ViewBookingDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
