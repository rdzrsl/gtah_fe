export const environment = {
  production: true,
  backendURL: "/service/public/api/",
  storageURL: "/service/public/storage/uploads"
};
