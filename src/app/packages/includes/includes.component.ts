import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { SharedService } from '../../services/shared.service';
import { CommonService } from 'src/app/services/common.service';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';

@Component({
  selector: 'app-includes',
  templateUrl: './includes.component.html',
  styleUrls: ['./includes.component.scss'],
  animations: [
    trigger('listStagger',[
      transition('* <=> *', [
        query(':enter', [
          style({ opacity: 0, transform: 'translateY(-15px)'}),
          stagger('50ms',
          animate('550ms ease-out',
          style({ opacity: 1, transform: 'translateY(0px)'})))
        ], {optional: true}),
        query(':leave', [
          animate('50ms',
          style({ opacity: 0}))
        ], {optional: true})

      ])
    ])
  ]
})
export class IncludesComponent implements OnInit, AfterViewInit {

  includes: any = [];
  totalElements: number;
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0;
  displayedColumns: string[] = ['num', 'name', 'status', 'action'];
  dataSource: MatTableDataSource<any>;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private common:CommonService,
    private dsvc:DataService,
    public shared: SharedService
    ) {
      this.dataSource = new MatTableDataSource(this.includes);
      this.shared.loader = true;
    }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    // this.getList(this.shared.pageNum,999);
    this.getList(this.shared.pageNum,this.shared.pageSize);
    
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getList(page,size) {

    let paginationUrl;

    if(page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }
    this.dsvc.getAll('packageincludes').subscribe(
      data => {

        this.includes = data;
        // this.includes = data['result']['data'];

        this.includes.forEach(function(item,index)  {
          item.row = index;
        });

        this.totalElements = this.includes.length;
        // console.log({'total':this.totalElements});
        this.dataSource.data = this.includes;
        this.dataSource.paginator = this.paginator;
        this.paginator.length = this.totalElements;
        this.dataSource.sort = this.sort;
        
        // UPDATE SHARED DATA
        this.dsvc.getAll('packageincludes/active').subscribe(
          res => {
            this.shared.allPackageIncludesData = res;
            // console.log({'includes shared.allPackageIncludesData':this.shared.allPackageIncludesData});
            this.shared.loader = false;
            
          }, err => {
            console.log({'err':err});
            this.shared.loader = false;
          });
          this.shared.loader = false;
    }),
    error => {
      this.shared.loader = false;
      alert('Data unreachable!');
      console.log({'err':error});
    };
  }

  removeItem(id, name) {
    if(confirm("Delete: "+name+'?')) {
      this.dsvc.delete('packageincludes', id).subscribe(
        data => {
          setTimeout(() => {
            this.common.showSuccess('Delete Successful!', '');
            this.getList(this.shared.pageNum,this.shared.pageSize);
          }, 1000);
      }),
      error => {
        this.common.showSuccess('Data unreachable!', '');
        console.log({'err':error});
      };
    } else {
    }
  }
}