import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-commissionconfig',
  templateUrl: './commissionconfig.component.html',
  styleUrls: ['./commissionconfig.component.scss']
})
export class CommissionconfigComponent implements OnInit {

  refId: any;
  getData: Object;
  commconfig: any = [];
  nullPointers: any = [];

  updateForm: FormGroup;

  name: FormControl; 
  total: FormControl; 
  hq: FormControl; 
  agent: FormControl; 
  consultant: FormControl; 
  ops: FormControl; 
  active: FormControl;
  created_at: any; 
  updated_at: any;
  isActive: any;
  cc_id: any;
  pageMode: string;
  complete: boolean;

  constructor(
    private router:Router,
    private dsvc:DataService,
    private toastr: ToastrService,
    public shared: SharedService
    ) {
      moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
      // this.shared.loader = true;
  }

  ngOnInit() {

    this.refId = this.router.url.split('/')[3];

    this.name = new FormControl();
    this.total = new FormControl();
    this.hq = new FormControl();
    this.agent = new FormControl();
    this.consultant = new FormControl();
    this.ops = new FormControl();
    this.active = new FormControl();

    this.updateForm = new FormGroup ({
      name : this.name,
      total : this.total,
      hq : this.hq,
      agent : this.agent,
      consultant : this.consultant,
      ops : this.ops,
      active : this.active,
    });

    this.updateForm.get('active').setValue(0);

    if(this.refId === 'add') {
      this.pageMode = 'ADD';
      this.shared.loader = false;
    }else {
      // this.shared.loader = true;
      this.getRow(this.refId);
      this.pageMode = 'EDIT';
    }
    this.checkReqValues();
  }

  getRow(id) {
    
    // Update ErrorMsg Service
    this.dsvc.getByID('commconfig',id)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
        this.getData = Rdata;
      
        // populate data
        this.cc_id = this.refId;
        this.updateForm.get('name').setValue(this.getData['name']);
        this.updateForm.get('total').setValue(this.getData['total']?this.getData['total']:'0');
        this.updateForm.get('hq').setValue(this.getData['hq']?this.getData['hq']:'0');
        this.updateForm.get('agent').setValue(this.getData['agent']?this.getData['agent']:'0');
        this.updateForm.get('ops').setValue(this.getData['ops']?this.getData['ops']:'0');
        // this.updateForm.get('consultant').setValue(this.getData['consultant']?this.getData['consultant']:0);

        this.updateForm.get('active').setValue((this.getData['active'] == 1)?true:false);

        this.isActive = this.getData['active'];
        this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
        this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

        this.checkReqValues();
        this.shared.loader = false;
      // }).bind(this));
    }, err => {
      this.shared.loader = false;
      console.log({'err':err});
    });
    
  }

  clear(elem) {
    this.updateForm.get(elem).setValue('');
    this.checkReqValues();
  }

  back(module) {
    this.router.navigate([module])
  }

  checkPercentage() {
    let sumTotal;
    let total = parseInt(this.total.value);
    let hq = parseInt(this.hq.value);
    let agent = parseInt(this.agent.value);
    // let consultant = parseInt(this.consultant.value);
    let ops = parseInt(this.ops.value);
    
    sumTotal = hq + agent + 2 + ops;

    if(sumTotal > total) {
      this.complete = false;
      this.toastr.error('Total percentage exceeded!', '');
    } else if(sumTotal < total) {
      this.complete = false;
      this.toastr.error('Total percentage does not match or lesser!', '');
    }

  }

  checkReqValues() {

    let reqVal: any = ['name','total', 'hq', 'agent', 'ops'];
    let nullPointers: any = [];

    for (var reqData of reqVal) {
      let elem = this.updateForm.get(reqData);

      if (elem.value == "" || elem.value == null) {
        elem.setValue(null)
        nullPointers.push(null)
      }
    }

    if (nullPointers.length > 0) {
      this.complete = false;
    } else {
      this.complete = true;
    }
// console.log(nullPointers.length);

    this.checkPercentage();
  }

  submit(formValues:any) {
    this.shared.loader = true;

    let body = {
      id: null,
      name: null,
      total: null,
      hq: null,
      agent: null,
      consultant: 2,
      ops: null,
      active: 0
    };
    
    body.name = formValues.name;
    body.total = parseInt(formValues.total);
    body.hq = parseInt(formValues.hq);
    body.agent = parseInt(formValues.agent);
    // body.consultant = parseInt(formValues.consultant); 
    body.ops = parseInt(formValues.ops);
    body.active = formValues.active;
    
    if(this.refId !== 'add') { // UPDATE
      body.id = this.refId;
    
    // console.log({'edit body':body});
      this.dsvc.update('commconfig', body, parseInt(this.refId))
      .subscribe(res => {
        console.log(res)
        this.toastr.success('Update Successful!', '');
        this.router.navigate(['protected/commissionconfig']);
        this.shared.loader = false;
      }),
      error => {
        console.log({'err':error.message});
        this.toastr.error('Error! '+error, '');
        this.shared.loader = false;
      };
    } else { // ADD

      // console.log({'add body':body});

      this.dsvc.store('commconfig',body)
      .subscribe(res => {
        console.log(res)
        this.toastr.success('Add Successful!', '');
        this.router.navigate(['protected/commissionconfig']);
        this.shared.loader = false;
      }),
      error => {
        console.log({'err':JSON.parse(error.message)});
        this.toastr.error('Error! '+error, '');
        this.shared.loader = false;
      };

    }
  }

}
