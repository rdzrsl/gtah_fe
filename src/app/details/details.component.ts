import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  user: Object;

  constructor(private dsvc:DataService, private route:ActivatedRoute) { 
    this.route.params.subscribe(
      params => this.user = params.id
    ),
    error => {
      alert('Data unreachable!');
      console.log({'err':error});
    };
  }

  ngOnInit() {
    // this.dsvc.getUser(this.user).subscribe(
    //   data => this.user = data
    // ),
    // error => {
    //   alert('Data unreachable!');
    //   console.log({'err':error});
    // };
  }

}
