import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import { SharedService } from '../services/shared.service';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../services/common.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { DataService } from '../services/data.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  currentUrl: string;
  isSupAdm: boolean = false;
  isHq: boolean = false;
  isLogin: boolean;
  creator: boolean;

  constructor(
    public shared:SharedService,
    private auth:AuthService,
    private http: HttpClient,
    private dsvc: DataService,
    private toastr: ToastrService,
    private common:CommonService, 
    private router:Router, 
    private route:ActivatedRoute) {

      this.isLogin = this.common.isLogin;
      this.creator = false;
         
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        // console.log({'e':e});
        
        this.shared.previousUrl = e.url;
        this.currentUrl = e.url.split('/')[2];
      }
    }),
    error => {
      alert('Error!');
      console.log({'err':error});
    };
  }
  
  ngOnInit() {
    if(localStorage.getItem('at'))
      this.getUserCred();
      
    if(this.shared.u_type == '1')
      this.isSupAdm = true;
    else if(this.shared.u_type == '2')
      this.isHq = true;

    if(localStorage.getItem('u_id') == '1')
      this.creator = true;
  }

  getUserTypes() {
    this.dsvc.getAll('usertypes/'+localStorage.getItem('ut')).subscribe(data => {
      this.shared.userTypesData = data;
    },
    error => {
      this.toastr.error('Error! '+error.statusText, '');
      this.shared.loader = false;
      this.router.navigate(['/']);
    });
    return this.shared.userTypesData;
  }

  getUserCred() {
    let headers = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('at'));
    // this.createAuthorizationHeader(headers);
    return this.http.get(environment.backendURL+'user/getap/'+localStorage.getItem('u_id'), { headers }).subscribe(
      data => {
        this.shared.co_id = data[0];
        this.shared.u_type = data[1];
        localStorage.setItem('ut',data[1]);
        // console.log({'shared.u_type':this.shared.u_type});
        if(localStorage.getItem('ut') == '1' || localStorage.getItem('ut') == '2') {
          this.shared.isHq = true;
          this.shared.isAgent = false;
        } else if(localStorage.getItem('ut') == '4') {
          this.shared.isHq = false;
          this.shared.isAgent = true;
        }
        this.getUserTypes();
      }
    ,
    error => {
      this.toastr.error('Error! '+error.statusText, '');
      this.shared.loader = false;
      this.router.navigate(['/']);
    });
  }

  userLogout() {
    this.auth.userLogout();
  }

}
