import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { SharedService } from 'src/app/services/shared.service';
import * as moment from 'moment-timezone';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { environment } from '../../../environments/environment';

export interface DialogData {
  src: string;
}

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  code: any;
  refId: any;
  adultComm: any;
  childbedComm: any;
  childnobedComm: any;
  infantComm: any;
  getData: any = [];
  media: any = [];
  src2modal: any;
  urlStorage = environment.storageURL;
  ccDetails: any = [];

  constructor(
    public dialog: MatDialog,
    private router:Router,
    private dsvc:DataService,
    public shared: SharedService) {
      moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
      this.shared.loader = true; 
      this.shared.u_type = parseInt(localStorage.getItem('ut'));
    }

  ngOnInit() {
    
    this.code = this.router.url.split('/')[2];
    this.getRow(this.code);
  }

  getRow(code) {
    
    let incArr = [];
    let exArr = [];
    this.shared.loader = true;

    // Update ErrorMsg Service
    this.dsvc.getByID('package/view',code, true)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
        this.getData = Rdata[0];
        // console.log({'getData':this.getData});

        this.refId = this.getData.id;

        if(this.shared.u_type == 1 || this.shared.u_type == 2 || this.shared.u_type == 4)
          this.getCCDetails(this.getData.cc_id);

        this.getData.updated_at = moment(this.getData.updated_at).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm a');
        this.getData.travel_date = moment(this.getData.travel_date).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');
        this.getData.return_date = moment(this.getData.return_date).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY');

        // console.log({'this.getData.includes_id':this.getData.includes_id});
        // console.log({'this.getData.excludes_id':this.getData.excludes_id});
        // console.log({'this.getData.includes_id json parse':JSON.parse(this.getData.includes_id)});
        // console.log({'this.getData.excludes_id json parse':JSON.parse(this.getData.excludes_id)});

        this.getData.includes_id = JSON.parse(this.getData.includes_id);
        this.getData.excludes_id = JSON.parse(this.getData.excludes_id);
        
        if(this.getData.includes_id) {
        this.getData.includes_id.forEach(el1 => {
            incArr.push(this.shared.getIncludeItemNameByID(el1));
          });
        }
        
        if(this.getData.excludes_id) {
          this.getData.excludes_id.forEach(el2 => {
            exArr.push(this.shared.getIncludeItemNameByID(el2));
          });
        }

        this.getData.includes_id = incArr;
        this.getData.excludes_id = exArr;

        // console.log(this.getData.includes_id);
        // console.log(this.getData.excludes_id);

        this.getAllMedia(this.getData.id);

        // this.shared.loader = false;
      // }).bind(this));
    }, err => {
      console.log({'err':err});
          this.shared.loader = false;
    });

  }

  getCCDetails(id) {
    
    // Update ErrorMsg Service
    this.dsvc.getByID('commconfig', id)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
        this.ccDetails = Rdata;
        // console.log({'ccDetails':this.ccDetails});

        this.adultComm = (parseFloat(this.getData.adult_price) * parseInt(this.ccDetails.agent) / 100).toFixed(2);
        this.childbedComm = (parseFloat(this.getData.childbed_price) * parseInt(this.ccDetails.agent) / 100).toFixed(2);
        this.childnobedComm = (parseFloat(this.getData.childnobed_price) * parseInt(this.ccDetails.agent) / 100).toFixed(2);
        this.infantComm = (parseFloat(this.getData.infant_price) * parseInt(this.ccDetails.agent) / 100).toFixed(2);

        this.shared.loader = false;
      // }).bind(this));
    }, err => {
      this.shared.loader = false;
      console.log({'err':err});
    });
    
  }

  getAllMedia(id) {
    let arrFileNames = [];

    this.shared.loader = true;
    // Update ErrorMsg Service
    this.dsvc.getByID('media/package', id, true)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
          this.media = Rdata[0];
          // console.log({'media':this.media});

          // this.media.forEach(el => {
          //   // console.log({'el.name':el.name});

          //   arrFileNames.push(el.name);
          
          // });

          // this.updateForm.controls.mediaFile.setValue(arrFileNames.join(", "));
          
          // }).bind(this));
          this.shared.loader = false;
        }, err => {
          console.log({'err':err});
          this.shared.loader = false;
          // this.shared.loader = false;
    });
  }

  openDialog(imgSrc): void {
    this.src2modal = this.urlStorage+'/package/'+this.refId+'/'+imgSrc;
    const dialogRef = this.dialog.open(ViewDialog, {
      width: '800px',
      // height: '600px',
      data: {'src': this.src2modal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}

@Component({
  selector: 'view-dialog',
  templateUrl: './view-dialog.html',
  styleUrls: ['./view.component.scss']
})
export class ViewDialog {

  constructor(
    public dialogRef: MatDialogRef<ViewDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
