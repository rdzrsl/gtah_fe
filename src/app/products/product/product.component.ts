import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  refId: any;
  getData: Object;
  products: any = [];
  nullPointers: any = [];

  updateForm: FormGroup;

  name: FormControl; 
  user: FormControl; 
  product_id: any; 
  co_id: any; 
  unit: FormControl; 
  cost_price: FormControl; 
  sale_price: FormControl; 
  // weight: FormControl; 
  // pen_postage: FormControl; 
  // west_postage: FormControl; 
  active: FormControl;
  created_at: any; 
  updated_at: any;
  isActive: any;
  pageMode: string;
  complete: boolean;

  constructor(
    private router:Router,
    private dsvc:DataService,
    private toastr: ToastrService,
    public shared: SharedService
    ) {
      moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
  }

  ngOnInit() {

    this.refId = this.router.url.split('/')[3];
    this.co_id = localStorage.getItem('co_id');

    this.name = new FormControl();
    this.user = new FormControl();
    // this.product_id = new FormControl();
    this.co_id = new FormControl(); 
    this.unit = new FormControl();
    // this.unit = new FormControl('', [Validators.pattern('[[:digit:]]')]);
    this.cost_price = new FormControl(); 
    this.sale_price = new FormControl(); 
    // this.weight = new FormControl(); 
    // this.pen_postage = new FormControl();
    // this.west_postage = new FormControl();
    this.active = new FormControl();

    this.updateForm = new FormGroup ({
      name : this.name,
      user : this.user,
      // product_id : this.product_id,
      co_id : this.co_id,
      unit : this.unit,
      cost_price : this.cost_price,
      sale_price : this.sale_price,
      // weight : this.weight,
      // pen_postage : this.pen_postage,
      // west_postage : this.west_postage,
      active : this.active,
    });
    // this.getProducts();
    // this.updateForm.get('active').setValue(false);

    if(this.refId === 'add') {
      this.pageMode = 'ADD';
    }else {
      this.getRow(this.refId);
      this.pageMode = 'EDIT';
    }
    this.checkReqValues();
  }

  getRow(id) {
    
    // Update ErrorMsg Service
    this.dsvc.getProductByID(id)
    .subscribe(
      Rdata => {
        // this.commonservice.errorHandling(Rdata, (function(){
        this.getData = Rdata['body'];
      
        // console.log(this.getData);
        
        // populate data
        this.updateForm.get('name').setValue(this.getData['name']);
        this.updateForm.get('user').setValue(this.getData['user']);
        this.product_id = this.refId;

        if(localStorage.getItem('co_id') === '1')
          this.co_id = this.getData['co_id'];

        // this.updateForm.get('product_id').setValue(JSON.parse(this.getData['product_id']));
        this.updateForm.get('co_id').setValue(this.getData['co_id']);
        this.updateForm.get('unit').setValue(this.getData['unit']);
        this.updateForm.get('cost_price').setValue(this.getData['cost_price']);
        this.updateForm.get('sale_price').setValue(this.getData['sale_price']);
        // this.updateForm.get('weight').setValue(this.getData['weight']);
        // this.updateForm.get('west_postage').setValue(this.getData['west_postage']);
        // this.updateForm.get('pen_postage').setValue(this.getData['pen_postage']);
        this.updateForm.get('active').setValue((this.getData['active'] == 1)?true:false);

        this.isActive = this.getData['active'];
        this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
        this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

        this.checkReqValues();
      // }).bind(this));
    }, err => {
      console.log({'err':err});
    });
    
  }

  clear(elem) {
    this.updateForm.get(elem).setValue('');
  }

  back(module) {
    this.router.navigate([module])
  }

  checkReqValues() {

    let name = "name";
    let unit = "unit";
    // let product_id = "product_id";
    let cost_price = "cost_price";
    let sale_price = "sale_price";
    let active = "active";

    let reqVal: any = [name, unit, cost_price, sale_price];
    let nullPointers: any = [];

    for (var reqData of reqVal) {
      let elem = this.updateForm.get(reqData);

      if (elem.value == "" || elem.value == null) {
        elem.setValue(null)
        nullPointers.push(null)
      }
    }

    if (nullPointers.length > 0) {
      this.complete = false;
    } else {
      this.complete = true;
    }

  }

  submit(formValues:any) {

    let body = {
      co_id: null,
      name: null,
      // pen_postage: null,
      cost_price: null,
      sale_price: null,
      product_id: null,
      unit: null,
      user: null,
      // weight: null,
      // west_postage: null,
      active: null
    };

    formValues.product_id = JSON.stringify(formValues.product_id);

    body.co_id = this.shared.co_id;
    body.user = this.shared.u_id;
    if(this.refId !== 'add') {
    
    body.name = formValues.name;
    // body.pen_postage = parseFloat(formValues.pen_postage);
    body.cost_price = parseFloat(formValues.cost_price);
    body.sale_price = parseFloat(formValues.sale_price);
    body.product_id = formValues.product_id;
    body.unit = parseInt(formValues.unit);
    // body.weight = formValues.weight;
    // body.west_postage = parseFloat(formValues.west_postage);
    body.active = formValues.active;
    
    // console.log({'edit body':body});
      this.dsvc.putProduct(body, parseInt(this.refId))
      .subscribe(res => {
        console.log(res)
        this.toastr.success('Update Successful!', '');
        this.router.navigate(['product']);
      }),
      error => {
        console.log({'err':error.message});
        this.toastr.error('Error! '+error, '');
      };
    } else {
    
      body.name = formValues.name;
      // body.pen_postage = parseFloat(formValues.pen_postage);
      body.cost_price = parseFloat(formValues.cost_price);
      body.sale_price = parseFloat(formValues.sale_price);
      body.product_id = formValues.product_id;
      body.unit = parseInt(formValues.unit);
      // body.weight = formValues.weight;
      // body.west_postage = parseFloat(formValues.west_postage);
      body.active = formValues.active;

      console.log({'add body':body});

      this.dsvc.addProduct(body)
      .subscribe(res => {
        console.log(res)
        this.toastr.success('Add Successful!', '');
        this.router.navigate(['product']);
      }),
      error => {
        console.log({'err':JSON.parse(error.message)});
        this.toastr.error('Error! '+error, '');
      };

    }
  }

}
