import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment-timezone';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';
// import { decode } from 'punycode';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  refId: any;
  lastInsertId: any;
  getData: Object;
  usertypes: any = [];
  banks: any = [];
  bankaccount: any = [];
  nullPointers: any = [];

  updateForm: FormGroup;

  name: FormControl;
  usertype: FormControl;
  email: FormControl;
  icno: FormControl;
  contact_no: FormControl;
  address: FormControl;
  active: FormControl;
  // password: FormControl;
  // password_confirmation: FormControl;
  bank_id: FormControl;
  account_no: FormControl;
  holder_name: FormControl;
  co_id: any;
  ba_id: any;
  user_id: any;
  created_at: any;
  updated_at: any;
  isActive: any;
  pageMode: string;
  complete: boolean;

  constructor(
    private router: Router,
    private dsvc: DataService,
    private toastr: ToastrService,
    private common: CommonService,
    public shared: SharedService
  ) {
    moment().tz("Asia/Kuala_Lumpur").format('YYYY-MM-DD h:m A');
    this.shared.loader = true;
  }

  ngOnInit() {

    this.refId = this.router.url.split('/')[3];
    this.co_id = localStorage.getItem('co_id');

    this.name = new FormControl();
    this.icno = new FormControl();
    this.email = new FormControl();
    this.address = new FormControl();
    this.contact_no = new FormControl();
    this.usertype = new FormControl();
    // this.password = new FormControl();
    // this.password_confirmation = new FormControl();
    this.bank_id = new FormControl();
    this.account_no = new FormControl();
    this.holder_name = new FormControl();
    this.active = new FormControl();

    this.updateForm = new FormGroup({
      name: this.name,
      icno: this.icno,
      email: this.email,
      address: this.address,
      contact_no: this.contact_no,
      usertype: this.usertype,
      // password: this.password,
      // password_confirmation: this.password_confirmation,
      bank_id: this.bank_id,
      account_no: this.account_no,
      holder_name: this.holder_name,
      active: this.active
    });

    this.getAllUserTypes();
    this.getAllBanks();

    if (this.refId === 'add') {
      this.pageMode = 'ADD';
    } else {
      this.getRow(this.refId);
      this.pageMode = 'EDIT';
    }
    this.checkReqValues();
  }

  getAllUserTypes() {
    this.shared.loader = true;
    // let arrUserTypes = [];
    this.dsvc.getUserTypes().subscribe(
      data => {
        this.usertypes = data;

        // this.usertypes.forEach(el => {
        //   if(el.id )
        // });
        this.shared.loader = false;
      }),
      error => {
        alert('Data unreachable!');
        console.log({ 'err': error });
        this.shared.loader = false;
      };
  }

  getAllBanks() {
    this.shared.loader = true;
    let arr: any = [];
    var bname = document.createElement('textarea');
    // console.log(bname);

    this.dsvc.getAll('bank').subscribe(
      data => {
        arr = data;

        arr.forEach(el => {
          bname.innerHTML = el.name;
          el.name = bname.value;
        });

        this.banks = data;
        this.shared.loader = false;
      }),
      error => {
        alert('Data unreachable!');
        console.log({ 'err': error });
        this.shared.loader = false;
      };
    // console.log({'banks':this.banks});
  }

  getBankAccountByID(id) {
    this.shared.loader = true;
    // console.log({'ba_id':id});
    this.bankaccount = [];

    this.dsvc.getByID('bankaccount', id).subscribe(
      data => {
        this.bankaccount = data;
        // console.log({'bankaccount':this.bankaccount});
        this.updateForm.get('bank_id').setValue(JSON.parse(this.bankaccount['bank_id']));
        this.updateForm.get('account_no').setValue(this.bankaccount['account_no']);
        this.updateForm.get('holder_name').setValue(this.bankaccount['holder_name']);

        this.shared.loader = false;
      }),
      error => {
        alert('Data unreachable!');
        console.log({ 'err': error });
        this.shared.loader = false;
      };
    // console.log({'banks':this.banks});
  }

  getRow(id) {
    this.shared.loader = true;

    // Update ErrorMsg Service
    this.dsvc.getUserByID(id)
      .subscribe(
        Rdata => {
          // this.commonservice.errorHandling(Rdata, (function(){
          this.getData = Rdata;

          // console.log(this.getData);

          // populate data
          // this.user_id = this.refId;
          this.updateForm.get('name').setValue(this.getData['name']);
          this.updateForm.get('email').setValue(this.getData['email']);
          this.updateForm.get('address').setValue(this.getData['address']);
          this.updateForm.get('usertype').setValue(JSON.parse(this.getData['type']));
          this.updateForm.get('contact_no').setValue(this.getData['contact_no']);
          this.updateForm.get('icno').setValue(this.getData['icno']);
          this.updateForm.get('active').setValue((this.getData['active'] == 1) ? true : false);

          this.isActive = this.getData['active'];
          this.created_at = moment(this.getData['created_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
          this.updated_at = moment(this.getData['updated_at']).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');

          if (this.getData['ba_id'] !== null) {
            this.ba_id = this.getData['ba_id'];
            this.getBankAccountByID(this.ba_id);
          }

          this.checkReqValues();
          this.shared.loader = false;
          // }).bind(this));
        }, err => {
          this.shared.loader = false;
          console.log({ 'err': err });
        });

  }

  validateUserData(type,value) {

    let dataBody = { "type": type, "value": value };

    if(value && value.length > 0) {
      this.shared.loader = true;
      this.dsvc.store('validateUserData', dataBody).subscribe(res => {
        
        if(res.body) {
          if(type == 'email' && value != "") {
            this.updateForm.get('email').setErrors({'incorrect': true});  
            this.common.showError('Email already exist', '');
          } else if(type == 'icno' && value != '') {
            this.updateForm.get('icno').setErrors({'incorrect': true});  
            this.common.showError('Identification Number already exist', '');
          }
        }
          this.shared.loader = false;
          this.checkReqValues();

      }),
      error => {
        this.shared.loader = false;
        console.log({ 'err': JSON.parse(error) });
        // this.toastr.error('Error! '+JSON.parse(error), '');
        this.common.showError(JSON.parse(error).errors, '');
      };
    }
  }

  clear(elem) {
    this.updateForm.get(elem).setValue('');
  }

  back(module) {
    this.router.navigate([module])
  }

  checkReqValues(ba?) {

    let reqVal: any = [];

    if (ba) {
      reqVal = ['name', 'usertype', 'contact_no', 'icno', 'email', 'address', 'bank_id', 'account_no', 'holder_name'];
    } else {
      reqVal = ['name', 'usertype', 'contact_no', 'icno', 'email', 'address'];
    }

    let nullPointers: any = [];

    for (var reqData of reqVal) {
      let elem = this.updateForm.get(reqData);

      if (elem.value == "" || elem.value == null) {
        elem.setValue(null)
        nullPointers.push(null)
      }
    }

    if (nullPointers.length > 0) {
      this.complete = false;
    } else {
      this.complete = true;
    }

    if (this.updateForm.get('email').invalid || this.updateForm.get('icno').invalid) {
      this.complete = false;
    } else {
      this.complete = true;
    }

  }

  checkBankAccountDetails(val) {

    if (val) {
      this.checkReqValues(1);
    }
  }

  submit(formValues: any) {

    // this.shared.loader = true;

    let bodyUser = {
      co_id: null,
      ba_id: null,
      name: null,
      type: null,
      id: null,
      address: null,
      icno: null,
      contact_no: null,
      email: null,
      // password: null,
      // password_confirmation: null,
      active: 0
    };

    let bodyBankAcc = {
      user_id: null,
      bank_id: null,
      account_no: null,
      holder_name: null
    }

    bodyUser.co_id = this.shared.co_id;

    if (this.refId !== 'add') {

      bodyUser.id = this.refId;
      bodyUser.name = formValues.name;
      bodyUser.type = parseInt(formValues.usertype);
      bodyUser.address = formValues.address;
      bodyUser.email = formValues.email;
      bodyUser.icno = formValues.icno;
      bodyUser.contact_no = formValues.contact_no;
      bodyUser.active = formValues.active;
      // bodyUser.password = formValues.password;
      // bodyUser.password_confirmation = formValues.password;

      if (this.ba_id) {
        bodyUser.ba_id = this.ba_id;
      }

      if (formValues.bank_id && formValues.account_no && formValues.holder_name) {
        bodyBankAcc.user_id = parseInt(this.refId);
        bodyBankAcc.bank_id = parseInt(formValues.bank_id);
        bodyBankAcc.account_no = formValues.account_no;
        bodyBankAcc.holder_name = formValues.holder_name;

        // UPDATE BANK ACCOUNT
        this.dsvc.update('bankaccount', bodyBankAcc).subscribe(res => {
          // console.log({ 'res bankaccount ': res });

          this.lastInsertId = res.body['last_insert_id'];

          if (this.lastInsertId) {
            bodyUser.ba_id = this.lastInsertId;
          } else {
            bodyUser.ba_id = this.ba_id;
          }
          this.shared.loader = false;

          // UPDATE USER
          this.dsvc.update('user', bodyUser, this.refId).subscribe(res => {
            // console.log({ 'res user with ba': res });
            // console.log({ 'user lastInsertId': this.lastInsertId });

            this.toastr.success('Update Successful!', '');
            this.router.navigate(['protected/users']);
          }),
            error => {
              console.log({ 'err': error.message });
              this.toastr.error('Error! ' + error, '');
            };

          //   this.toastr.success('Update Successful!', '');
          //   this.router.navigate(['protected/users']);
          this.shared.loader = false;
        }),
          error => {
            this.shared.loader = false;
            console.log({ 'err': error.message });
            this.toastr.error('Error! ' + error, '');
          };

      } else {

        // UPDATE USER
        this.dsvc.update('user', bodyUser, this.refId).subscribe(res => {
          console.log({ 'res user without ba': res });
          this.shared.loader = false;

          this.toastr.success('Update Successful!', '');
          this.router.navigate(['protected/users']);
        }),
          error => {
            this.shared.loader = false;
            console.log({ 'err': error.message });
            this.toastr.error('Error! ' + error, '');
          };

      }

    } else {

      bodyUser.name = formValues.name;
      bodyUser.type = parseInt(formValues.usertype);
      bodyUser.address = formValues.address;
      bodyUser.email = formValues.email;
      bodyUser.icno = formValues.icno;
      bodyUser.contact_no = formValues.contact_no;
      bodyUser.active = formValues.active;
      // bodyUser.password = formValues.password;
      // bodyUser.password_confirmation = formValues.password_confirmation;

      if (this.ba_id)
        bodyUser.ba_id = this.ba_id;

      bodyBankAcc.bank_id = parseInt(formValues.bank_id);
      bodyBankAcc.account_no = formValues.account_no;
      bodyBankAcc.holder_name = formValues.holder_name;

      // console.log({'add bodyUser':bodyUser});,
      // bodyUser.type = "usermail";

      // if(formValues.password)
      this.dsvc.store('register', bodyUser).subscribe(res => {
        // console.log(res);
        // this.common.errorHandling(res, (function () {
          console.log(res);
          
          this.dsvc.store('mailer', bodyUser).subscribe(
            res => {
              this.toastr.success(res.body['message'], '');
              this.shared.loader = false;
            }),
            error => {
              this.shared.loader = false;
              this.toastr.error('Error! '+error, '');
              console.log({'err':error});
          };

          this.lastInsertId = res.body['last_insert_id'];
          // console.log({'lastInsertId':this.lastInsertId});

          if (formValues.bank_id && formValues.account_no && formValues.holder_name) {

            bodyBankAcc.user_id = this.lastInsertId;
            // console.log({'add bodyBankAcc':bodyBankAcc});
            this.shared.loader = false;

            this.dsvc.store('bankaccount', bodyBankAcc)
              .subscribe(res => {
                // console.log(res)
                this.shared.loader = false;
                this.toastr.success('Banking Details Add Successful!', '');
                this.router.navigate(['protected/users']);
              }),
              error => {
                this.shared.loader = false;
                console.log({ 'err': error.message });
                this.toastr.error('Error! ' + error, '');
              };
          }

          this.shared.loader = false;

          this.common.showSuccess('Registration Successful!', '');
          this.router.navigate(['protected/users']);

        // }).bind(this));
      }),
        error => {
          this.shared.loader = false;
          console.log({ 'err': JSON.parse(error) });
          // this.toastr.error('Error! '+JSON.parse(error), '');
          this.common.showError(JSON.parse(error).errors, '');
        };

    }
  }
}
