import { Router, NavigationEnd} from '@angular/router';
import { Component } from '@angular/core';
import * as moment from 'moment-timezone';
import { SharedService } from './../services/shared.service';
import { CommonService } from './../services/common.service';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.scss']
})
export class ProtectedComponent {
  title = 'cosagent';
  currentUrl: string;
  // moment = require('moment-timezone');

  constructor(private router:Router, public shared: SharedService, private common: CommonService) {
    router.events.subscribe(e => {
      // this.shared.loader = false;
      if (e instanceof NavigationEnd) {
        this.currentUrl = e.url;
        moment.tz.setDefault("Asia/Kuala_Lumpur");
        moment().format('YYYY-MM-DD h:m A');
        // console.log({'app date':moment().format('YYYY-MM-DD h:mm A')});
      }
    }),
    error => {
      alert('Error!');
      console.log({'err':error});
    };
   }

}
