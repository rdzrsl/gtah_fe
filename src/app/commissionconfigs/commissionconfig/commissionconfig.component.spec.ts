import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissionconfigComponent } from './commissionconfig.component';

describe('CommissionconfigComponent', () => {
  let component: CommissionconfigComponent;
  let fixture: ComponentFixture<CommissionconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommissionconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissionconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
