import { Component, OnInit } from '@angular/core';
import { trigger,style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
  animations: [
    trigger('listStagger',[
      transition('* <=> *', [
        query(':enter', [
          style({ opacity: 0, transform: 'translateY(-15px)'}),
          stagger('50ms',
          animate('550ms ease-out',
          style({ opacity: 1, transform: 'translateY(0px)'})))
        ], {optional: true}),
        query(':leave', [
          animate('50ms',
          style({ opacity: 0}))
        ], {optional: true})

      ])
    ])
  ]
})
export class CompaniesComponent implements OnInit {

  companies: Object;

  constructor(private dsvc:DataService) { }

  ngOnInit() {
    this.dsvc.getCompanies().subscribe(
      data => this.companies = data['body']
    ),
    error => {
      // alert('Data unreachable!');
      console.log({'err':error});
    };
  }

}
