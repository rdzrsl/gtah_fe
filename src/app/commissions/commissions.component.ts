import { Component, OnInit, ViewChild, AfterViewInit, Inject, ViewChildren, QueryList } from '@angular/core';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
import { DataService } from '../services/data.service';
import { SharedService } from '../services/shared.service';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment-timezone';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../services/common.service';
import { Ng4FilesStatus, Ng4FilesSelected, Ng4FilesService, Ng4FilesConfig } from '../ng4-files';
import { environment } from 'src/environments/environment';

export interface DialogData {
  bankname: string;
  holder_name: string;
  account_no: any;
  agent_amt: any;
  code: string;
  ref_no: string;
  agent_status: any;
  created_at: any;
}

@Component({
  selector: 'app-commissions',
  templateUrl: './commissions.component.html',
  styleUrls: ['./commissions.component.scss']
})

export class CommissionsComponent implements OnInit {

  commissions: any = [];
  egtcommission: any = [];
  totalElements: number;
  totalElements1: number;
  seqNo = 0;
  seqPageNum = 0;
  seqPageSize = 0;
  displayedColumns: string[] = ['num', 'ref_no', 'name', 'amount', 'pay_status', 'booking_status', 'created_at', 'action'];
  displayedColumns1: string[] = ['num', 'ref_no', 'amount', 'created_at'];
  dataSource: MatTableDataSource<any>;
  dataSource1: MatTableDataSource<any>;
  agentID: any;
  urlStorage = environment.storageURL;

  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>(); 
  @ViewChild(MatSort) sort: MatSort;
  // @ViewChild(MatPaginator) paginator1: MatPaginator;
  @ViewChild(MatSort) sort1: MatSort;
  userType: number;
  media: any;
  ng4FilesService: any;
  monthVal: any;

  constructor(
    public dialog: MatDialog,
    private dsvc: DataService,
    private toastr: ToastrService,
    public shared: SharedService) {
    this.shared.getPreviousUrl();
    //  this.shared.checkSession();;
    this.dataSource = new MatTableDataSource(this.commissions);
    this.dataSource1 = new MatTableDataSource(this.egtcommission.list);
    this.shared.loader = true;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    this.dataSource1.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }

    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }
  }

  ngOnInit() {
    
    this.agentID = parseInt(localStorage.getItem('u_id'));
    this.userType = parseInt(localStorage.getItem('ut'));
    this.monthVal = parseInt(moment().tz("Asia/Kuala_Lumpur").format('M'));

    this.getList(this.shared.pageNum, this.shared.pageSize);

    // if(this.agentID == 1)
    //   this.getEGTCommission(this.shared.pageNum, this.shared.pageSize);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator.toArray()[0];
    this.dataSource.sort = this.sort;
    this.dataSource1.paginator = this.paginator.toArray()[1];
    this.dataSource1.sort = this.sort1;
  }

  monthSel(value) {

    if(!value) {
      this.monthVal = moment().tz("Asia/Kuala_Lumpur").format('M');
    } else {
      this.monthVal = value;
    }
    // console.log(this.monthVal);

    this.getList(this.shared.pageNum, this.shared.pageSize);

    // if(this.agentID == 1)
    //   this.getEGTCommission(this.shared.pageNum, this.shared.pageSize);
  }

  getList(page, size) {

    this.shared.loader = true;
    this.commissions = [];
    this.egtcommission = [];
    this.dataSource.data = [];
    this.dataSource1.data = [];

    let paginationUrl;

    if (page && size) {
      paginationUrl = `?page=${page}&size=${size}`;
    } else {
      paginationUrl = '';
    }

    this.dsvc.getAll('commission/user/' + this.agentID + '/' + this.userType + '/' +this.monthVal).subscribe(
      res => {
        this.commissions = res['data'];
        this.egtcommission = res['egt'];
        // console.log({'egtcommission':this.egtcommission});

        if (res['total'] != 0) {
          this.commissions.forEach((item, index) => {
            item.row = index;
              // console.log(item.name);

            // if (item.user_id == 1) {
            //   item.name = "EGT";
            // } else if (item.user_id == 2) {
            //   item.name = "HQ";
            // } else if (item.user_id == 26) {
            //   item.name = "Operations";
            // }
            // if (item.agent_status == 1) {
            //   this.getMedia(item.booking_id);
            //   item.pop = this.media;
            //   console.log(item.pop);

            // }
            item.created_at = moment(item.created_at).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
          });

          this.totalElements = res['total'];
          this.dataSource.data = this.commissions;
          this.dataSource.paginator = this.paginator.toArray()[0];
          this.dataSource.sort = this.sort;
        }

        if (this.egtcommission && this.egtcommission.total != 0) {
          this.egtcommission.list.forEach((item, index) => {
            item.row = index;

            item.created_at = moment(item.created_at).tz("Asia/Kuala_Lumpur").format('DD-MM-YYYY h:mm A');
          });

          this.totalElements1 = this.egtcommission.total;
          this.dataSource1.data = this.egtcommission.list;
          this.dataSource1.paginator = this.paginator.toArray()[1];
          this.dataSource1.sort = this.sort1;
          // console.log({ 'totalElements1': this.totalElements1 });
          // console.log({ 'dataSource1': this.dataSource1 });
          // console.log({ 'paginator1': this.paginator1 });
          // console.log({ 'sort1': this.sort1 });
        }

        this.shared.loader = false;
      }),
      error => {
        this.shared.loader = false;
        alert('Data unreachable!');
        console.log({ 'err': error });
      };
      this.shared.loader = false;
  }

  getEGTCommission(page?, size?) {

    let paginationUrl;
    let getUrl = 'commission/egt';
  
      if(page && size) {
        paginationUrl = `?page=${page}&size=${size}`;
      } else {
        paginationUrl = '';
      }

    // console.log({'getUrl':getUrl});
    
    this.dsvc.getAll(getUrl+paginationUrl).subscribe(
      data => {
          this.egtcommission = data['data']['result']['data'];
          this.egtcommission.total = this.numberWithCommas(data['total']);
          this.egtcommission.forEach(el => {
            el.created_at = moment(el.created_at).format('DD-MM-YYYY h:mm A');
          });
        console.log({'this.egtcommission':this.egtcommission});
        this.shared.loader = false;
        
    }),
    error => {
      this.shared.loader = false;
      console.log({'err':error});
    };
  }

  numberWithCommas(x) {
      var parts = x.toString().split(".");
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return parts.join(".");
  }

  viewReceipt(id) {

    this.media = '';

    this.shared.loader = true;
    
    this.dsvc.getByID('media/payment/commission', id)
      .subscribe(
        Rdata => {
          this.media = this.urlStorage + '/' + Rdata[0].path + Rdata[0].name;
          window.open(this.media, '_blank');

          this.shared.loader = false;
        }, err => {
          console.log({ 'err': err });
          this.shared.loader = false;
        });

  }

  openDialog(content, userType?): void {
    // console.log({'content':content});
    
    if(userType)
      content.uType = userType;
    // if(folderName == 'payment') 
    //   this.src2modal = '/payment/'+refid+'/'+imgSrc;
    // else if(folderName == 'booking') 
    //   this.src2modal = '/booking/'+refid+'/'+imgSrc;

    const dialogRef = this.dialog.open(PaymentCommissionDialog, {
      width: '800px',
      // height: '600px',
      data: content
    });

    dialogRef.afterClosed().subscribe(result => {

      this.getList(this.shared.pageNum, this.shared.pageSize);
      // console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}

@Component({
  selector: 'payment-dialog',
  templateUrl: './payment-dialog.html',
  styleUrls: ['./commissions.component.scss']
})
export class PaymentCommissionDialog {

  updateForm: FormGroup;
  paymentFile: FormControl;
  reference: FormControl;
  pay_amt: FormControl;
  contentData: any = [];
  selFiles: any[];
  selectedPaymentFiles: string[];

  public fileConfig: Ng4FilesConfig = {
    acceptExtensions: [
      'docx', 
      'DOCX', 
      'doc', 
      'DOC', 
      'pdf', 
      'PDF', 
      'png', 
      'PNG', 
      'jpg', 
      'JPG', 
      'jpeg',
      'JPEG'
    ],
    maxFilesCount: 5,
    maxFileSize: 5120000,
    totalFilesSize: 5120000
  };

  constructor(
    private dsvc: DataService,
    private toastr: ToastrService,
    public common: CommonService,
    public shared: SharedService,
    private ng4FilesService: Ng4FilesService,
    public dialogRef: MatDialogRef<PaymentCommissionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    this.ng4FilesService.addConfig(this.fileConfig, 'fileConfig');

    this.pay_amt = new FormControl();
    this.paymentFile = new FormControl();
    this.reference = new FormControl();

    this.updateForm = new FormGroup({
      pay_amt: this.pay_amt,
      paymentFile: this.paymentFile,
      reference: this.reference,
    });

    this.contentData = data;
    // console.log({'contentData':this.contentData});

    this.updateForm.get('pay_amt').setValue(parseFloat(this.contentData.amount));

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  paymentFileSelect(selectedPaymentFiles: Ng4FilesSelected): void {

    let mFileSize = this.fileConfig.maxFileSize;

    let fileExtn = selectedPaymentFiles.files[0].name.split('.')[1];
    // let chkFileExtn = this.resFileExtn.filter(fData => fData === fileExtn.toLowerCase());

    if (selectedPaymentFiles.status === Ng4FilesStatus.STATUS_SUCCESS) {
      if (selectedPaymentFiles.files.length > 0 && mFileSize) {
        // if (selectedPaymentFiles.files[0].size <= mFileSize) {

        // Check File extn  
        // if (chkFileExtn.length > 0){
        // this.filesResult.en.size = selectedPaymentFiles.files[0].size;

        // this.selectedPaymentFiles = selectedPaymentFiles.files[0].name;
        let arrFileNames = [];
        this.selFiles = [];
        let file2convert;
        var file: File;
        let tempArr = [];
        let extension = null;
        let mediaType = null;

        selectedPaymentFiles.files.forEach(el => {

          arrFileNames.push(el.name);
          extension = el.name.split('.')[1];
          mediaType = this.getFileCode(extension);

          file = el;
          let myReader: FileReader = new FileReader();

          myReader.onloadend = (e) => {
            file2convert = myReader.result;
            tempArr.push({"path":null, "name":el.name, "size":el.size, "type":el.type, "ext":extension, "code":mediaType, "src":file2convert});
          }
          myReader.readAsDataURL(file);
        });

        this.selFiles = tempArr;
        // console.log({'selFiles':this.selFiles});



        this.updateForm.controls.paymentFile.setValue(arrFileNames.join(", "));

        // if (selectedPaymentFiles.files) {
        // }
        // this.checkReqValues();
        // this.showImg = true;
        //  }else {
        //   this.toastr.error('File Extension not match','');
        //  }    
      } else {
        this.toastr.error('File Size Exceed maximum file size', '');
      }

    } else if (selectedPaymentFiles.status === Ng4FilesStatus.STATUS_MAX_FILES_COUNT_EXCEED) {
      this.toastr.error('Maximum files count exceed.Please upload one file', '');
    } else if (selectedPaymentFiles.status === Ng4FilesStatus.STATUS_MAX_FILE_SIZE_EXCEED) {
      this.toastr.error('Maximum files size exceed', '');
      // }else if(selectedPaymentFiles.status === Ng4FilesStatus.STATUS_NOT_MATCH_EXTENSIONS){
      //   this.toastr.error('File Extension not match','');
    }
    // }

    this.selectedPaymentFiles = Array.from(selectedPaymentFiles.files).map(file => file.name);
  }

  getFileCode(ext) {
    let exist, res;

    exist = this.fileConfig.acceptExtensions.includes(ext.toLowerCase());
    
    res = exist;

    if(exist) {
      switch (ext) {
        case 'docx':
          res = 'word';
          break;

        case 'doc':
          res = 'word';
          break;

        case 'pdf':
          res = 'pdf';
          break;

        case 'png':
          res = 'image';
          break;

        case 'jpg':
          res = 'image';
          break;

        case 'jpeg':
          res = 'image';
          break;
      
        default:
          break;
      }
    }

    return res;
  }

  submit(formValues: any) {
    // console.log({'formValues':formValues});

    let paymentBody = {
      id: null,
      reference: formValues.reference,
      commission_id: this.contentData.id,
      booking_id: this.contentData.booking_id,
      paid_amt: parseFloat(this.contentData.amount),
      total_amt: parseFloat(this.contentData.amount),
      type: 2,
      // user_type: this.contentData.uType,
      uploadtype: 'payment',
      files: null
    };

    paymentBody.files = this.selFiles;
    console.log({'paymentBody':paymentBody});

    // PAYMENT
    this.dsvc.store('payment/commission', paymentBody)
      .subscribe(res => {
        // console.log({'res':res});
        this.toastr.success('Payment Update Successful!', '');
        paymentBody.id = res.body['last_insert_id'];

        if (this.selFiles.length != 0) { // Payment
          this.dsvc.store('media', paymentBody).subscribe(
            res => {
              this.toastr.success('Payment file(s) upload successful!', '');
              this.shared.loader = false;
            }),
            error => {
              this.shared.loader = false;
              this.toastr.error('Error! ' + error, '');
              console.log({ 'err': error });
            };
        }

        this.shared.loader = false;
      },
        error => {
          this.toastr.error('Error! ' + error, '');
          this.shared.loader = false;

        });
    this.dialogRef.close();


  }

}